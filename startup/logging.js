require("express-async-errors");
const winston = require("winston");

module.exports = function () {
  winston.add(
    new winston.transports.File(
      {
        filename: "unCaughtExceptions.log",
        handleExceptions: true,
        dirname: "log",
      },
      { exitOnError: true }
    )
  );

  process.on("unhandledRejection", (exception) => {
    throw exception;
  });

  winston.add(
    new winston.transports.File({ filename: "async.log", dirname: "log" })
  );
};
