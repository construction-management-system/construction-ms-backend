// third-party imports
const express = require("express");
// project imports
const errorHandler = require("../middlewares/errorHandler");
const { isAuth } = require("../middlewares/isAuth");
const auth_router = require("../routes/auth");
const employee_router = require("../routes/employee");
const job_router = require("../routes/job");
const role_router = require("../routes/role");
const shift_router = require("../routes/shift");

module.exports = (app) => {
  // middlewares
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());
  

  // routes
  app.use("/login", auth_router);
  app.use("/employee", employee_router);
  app.use("/role", role_router);
  app.use("/job", job_router);
  app.use("/shift", shift_router);

  // express error handler middleware
  app.use(errorHandler);
};
