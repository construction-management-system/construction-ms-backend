// third-party imports
const express = require("express");

// intializing app
const app = express();

// require("./startup/logging")();
require("./startup/routes")(app);

// set port
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server running on http://localhost:${PORT}`);
});
