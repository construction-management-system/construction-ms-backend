const oracledb = require("oracledb");
const { ORACLE_CONFIG } = require("../config/config");
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

module.exports = async ({ query, parameters, options }) => {
  let connection;
  try {
    connection = await oracledb.getConnection(ORACLE_CONFIG);

    await connection.execute(
      `ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'`
    );

    if (options.getRequest) {
      const sql = `
      BEGIN 
        ${query};
      END;
      `;
      const params = {
        ...parameters,
        return_cursor: { dir: oracledb.BIND_OUT, type: oracledb.CURSOR },
      };

      const result = await connection.execute(sql, params);
      const rows = await result.outBinds.return_cursor.getRows(
        options.rowCount
      );
      return rows;
    } else {
      const {
        outBinds: { key_array, value_array },
      } = await connection.execute(
        `
      DECLARE
      counter number := 0;
      response_object   VALIDATION_PACKAGE.increamental_object;
      key_array         increamental_array := increamental_array();
      value_array       increamental_array := increamental_array();
      object_item       VARCHAR2 (512);
      BEGIN 
        response_object := ${query};
        object_item := response_object.FIRST;
        FOR el IN 1..response_object.COUNT LOOP
          key_array.EXTEND;
          value_array.EXTEND;
          :key_array(el) := object_item;
          :value_array(el) := response_object(object_item);
          object_item := response_object(object_item);
        END LOOP;
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN
            :key_array(1) := 'ERROR: ';
            :value_array(1) :=  'Unique constraint error';
          WHEN OTHERS THEN 
              :key_array(1) := 'ERROR: ';
              :value_array(1) :=  DBMS_UTILITY.FORMAT_ERROR_STACK;
      END;
      `,
        {
          ...parameters,
          key_array: {
            dir: oracledb.BIND_OUT,
            type: oracledb.STRING,
            val: [],
            maxArraySize: 50,
          },
          value_array: {
            dir: oracledb.BIND_OUT,
            type: oracledb.STRING,
            val: [],
            maxArraySize: 50,
          },
        }
      );
      const result = {};
      for (let index = 0; index < key_array.length; index++) {
        result[key_array[index]] = value_array[index];
      }
      return result;
    }
  } catch (err) {
    switch (err.errorNum) {
      case 1403:
        return { "error: ": "no record found" };
      case 6502:
        return { "error: ": "numeric or value error" };
    }
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
};
