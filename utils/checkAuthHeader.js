const jwt = require('jsonwebtoken')
const { JWT_SECRET_KEY } = require('../config/config')

module.exports.checkAuthHeader = (headers) => {
    const authHeader = headers.authorization;
    if (authHeader) {
        // Bearer
        const token = authHeader.split("Bearer ")[1];
        if (token) {
            try {
                const user = jwt.verify(token, JWT_SECRET_KEY);
                return user;
            } catch (error) {
                return ({ error: "Invalid/Expire token" });
            }
        }
        return ({ error: "authorization token must be in 'Bearer token' format" });
    }
    return ({ error: "authorization token must be provided" });
};

