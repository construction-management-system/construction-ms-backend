const express = require("express");
const { body } = require("express-validator");
const {
  register,
  remove,
  update,
  getLimited,
} = require("../controllers/shift");

const router = express.Router();

// register route
router.post("/", register);

// get shift
router.get("/:user_id", getLimited);

// update route
router.put("/:shift_id", update);

// delete route
router.delete("/:shift_id", remove);

module.exports = router;
