const express = require("express");
const { body } = require("express-validator");
const { login } = require("../controllers/auth");

const router = express.Router();

// login post route
router.post(
  "/",
  [
    body("email")
      .trim()
      .notEmpty()
      .withMessage("email must be provided")
      .toLowerCase()
      .isEmail()
      .withMessage("email must be valid")
      .normalizeEmail(),
    body("password").notEmpty().withMessage("password must be provided "),
  ],
  login
);

module.exports = router;
