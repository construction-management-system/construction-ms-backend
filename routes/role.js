const express = require("express");
const { body } = require("express-validator");
const { register, remove, update, getLimited } = require("../controllers/role");

const router = express.Router();

// register route
router.post("/", register);

// get limeted Role
router.get("/:user_id", getLimited);

// update route
router.put("/:role_id", update);

// delete route
router.delete("/:role_id", remove);

module.exports = router;
