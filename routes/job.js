const express = require("express");
const { body } = require("express-validator");
const { register, remove, update, getLimited } = require("../controllers/job");

const router = express.Router();

// register route
router.post("/", register);

// get limeted Job
router.get("/:user_id", getLimited);

// update route
router.put("/:job_id", update);

// delete route
router.delete("/:job_id", remove);

module.exports = router;
