const run = require("../utils/db");

module.exports.register = async (req, res) => {
  const { user_id, role_name } = req.body;
  const myQuery = {
    query: `ROLE_PACKAGE.REGISTER_ROLE(:user_id, :role_name)`,
    parameters: {
      user_id,
      role_name,
    },
    options: {
      getRequest: false,
    },
  };
    const result = await run(myQuery);
    console.log(result);
    return res.json(result);
};

module.exports.getLimited = async (req, res) => {
  const { user_id } = req.params;
  const myQuery = {
    query: `ROLE_PACKAGE.GET_ROLES(:user_id, :return_cursor)`,
    parameters: {
      user_id,
    },
    options: {
      getRequest: true,
      rowCount: 5,
    },
  };
    const result = await run(myQuery);
    return res.json(result);
};

module.exports.update = async (req, res) => {
  const { user_id, role_name } = req.body;
  const { role_id } = req.params;
  const myQuery = {
    query: `ROLE_PACKAGE.UPDATE_ROLE(:user_id, :role_id,:role_name)`,
    parameters: {
      user_id,
      role_id,
      role_name,
    },
    options: {
      getRequest: false,
    },
  };
    const result = await run(myQuery);
    console.log(result);
    return res.json(result);

};

module.exports.remove = async (req, res) => {
  const { user_id } = req.body;
  const { role_id } = req.params;
  const myQuery = {
    query: `ROLE_PACKAGE.DELETE_ROLE(:user_id, :role_id)`,
    parameters: {
      user_id,
      role_id,
    },
    options: {
      getRequest: false,
    },
  };
    const result = await run(myQuery);
    console.log(result);
    return res.json(result);
};
