const run = require("../utils/db");

module.exports.register = async (req, res) => {
  const { user_id, shift_name } = req.body;
  const myQuery = {
    query: `SHIFT_PACKAGE.REGISTER_SHIFT(:user_id, :shift_name)`,
    parameters: {
      user_id,
      shift_name,
    },
    options: {
      getRequest: false,
    },
  };
  console.log(req.body);
  const result = await run(myQuery);
  console.log(result);
  return res.json(result);
};

module.exports.getLimited = async (req, res) => {
  const { user_id } = req.params;
  const myQuery = {
    query: `SHIFT_PACKAGE.GET_SHIFTS(:user_id, :return_cursor)`,
    parameters: {
      user_id,
    },
    options: {
      getRequest: true,
      rowCount: 5,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};

module.exports.update = async (req, res) => {
  const { user_id, shift_name } = req.body;
  const { shift_id } = req.params;
  const myQuery = {
    query: `SHIFT_PACKAGE.UPDATE_SHIFT(:user_id, :shift_id,:shift_name)`,
    parameters: {
      user_id,
      shift_id,
      shift_name,
    },
    options: {
      getRequest: false,
    },
  };
  const result = await run(myQuery);
  console.log(result);
  return res.json(result);
};

module.exports.remove = async (req, res) => {
  const { user_id } = req.body;
  const { shift_id } = req.params;
  const myQuery = {
    query: `SHIFT_PACKAGE.DELETE_SHIFT(:user_id, :shift_id)`,
    parameters: {
      user_id,
      shift_id,
    },
    options: {
      getRequest: false,
    },
  };
  const result = await run(myQuery);
  console.log(result);
  return res.json(result);
};
