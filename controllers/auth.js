const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { validationResult } = require("express-validator");
const { JWT_SECRET_KEY } = require("../config/config");
const run = require("../utils/db");

module.exports.login = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(403).json({
      errors: errors.array().map((error) => {
        return { message: error.msg, attribute: error.param };
      }),
    });
  const { email, password } = req.body;
  const myQuery = {
    query: `EMPLOYEE_PACKAGE.GET_EMPLOYEE(:user_id, :email, :return_cursor)`,
    parameters: {
      user_id: null,
      email: email.trim(),
    },
    options: {
      getRequest: true,
      rowCount: 1,
    },
  };
  const result = await run(myQuery);
  if (!result?.length)
    return res.json({ error: "password or email is incorrect" });

  const match = await bcrypt.compare(password, result[0].PASSWORD);
  if (!match) {
    return res.json({ error: "password is incorrect" });
  }
  const token = await jwt.sign({ ...result }, JWT_SECRET_KEY, {
    expiresIn: "8h",
  });
  return res.json({ ...result, token });
};
