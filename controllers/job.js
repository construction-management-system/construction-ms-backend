const run = require("../utils/db");

module.exports.register = async (req, res) => {
  const { user_id, job_name } = req.body;
  const myQuery = {
    query: `JOB_PACKAGE.REGISTER_JOB(:user_id, :job_name)`,
    parameters: {
      user_id: user_id ? user_id : null,
      job_name: job_name ? job_name : null,
    },
    options: {
      getRequest: false,
    },
  };
  console.log(user_id, job_name);
  const result = await run(myQuery);
  console.log(result)
  return res.json(result);
};

module.exports.getLimited = async (req, res) => {
  const { user_id } = req.params;
  const myQuery = {
    query: `JOB_PACKAGE.GET_JOBS(:user_id, :return_cursor)`,
    parameters: {
      user_id,
    },
    options: {
      getRequest: true,
      rowCount: 5,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};

module.exports.update = async (req, res) => {
  const { user_id, job_name } = req.body;
  const { job_id } = req.params;
  const myQuery = {
    query: `JOB_PACKAGE.UPDATE_JOB(:user_id, :job_id,:job_name)`,
    parameters: {
      user_id,
      job_id,
      job_name,
    },
    options: {
      getRequest: false,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};

module.exports.remove = async (req, res) => {
  const { user_id } = req.body;
  const { job_id } = req.params;
  const myQuery = {
    query: `JOB_PACKAGE.DELETE_JOB(:user_id, :job_id)`,
    parameters: {
      user_id,
      job_id,
    },
    options: {
      getRequest: false,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};
