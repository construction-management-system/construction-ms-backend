const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { validationResult } = require("express-validator");
const { JWT_SECRET_KEY } = require("../config/config");
const run = require("../utils/db");

module.exports.register = async (req, res) => {
  const {
    user_id,
    country,
    city,
    district,
    home_number,
    map_location,
    phone,
    email,
    first_name,
    last_name,
    ssn,
    photo,
    hire_date,
    has_password,
    password,
    two_factor,
    role_id,
    shift_id,
    job_id,
    salary,
    contract_file,
    cv_link,
  } = req.body;
  const myQuery = {
    query: `EMPLOYEE_PACKAGE.REGISTER_EMPLOYEE(R_USER_ID => :user_id, R_COUNTRY =>  :country, R_CITY =>  :city, R_DISTRICT => :district, R_HOME_NUMBER =>  :home_number, R_MAP_LOCATION => :map_location,R_PHONE => :phone, R_EMAIL => :email, R_FIRST_NAME => :first_name, R_LAST_NAME => :last_name, R_SSN => :ssn, R_PHOTO => :photo,R_HIRE_DATE => :hire_date, R_HAS_PASSWORD => :has_password,R_PASSWORD => :password, R_TWO_FACTOR => :two_factor, R_ROLE_ID => :role_id, R_SHIFT_ID => :shift_id, R_JOB_ID => :job_id, R_SALARY => :salary, R_CONTRACT_FILE => :contract_file,R_CV_LINK => :cv_link)`,
    parameters: {
      user_id: user_id ? user_id : null,
      country: country ? country : "NULL",
      city: city ? city : "NULL",
      district: district ? district : "NULL",
      home_number: home_number ? home_number : null,
      map_location: map_location ? map_location : "NULL",
      phone: phone ? phone : "NULL",
      email: email ? email : null,
      first_name: first_name ? first_name : "NULL",
      last_name: last_name ? last_name : "NULL",
      ssn: ssn ? ssn : null,
      photo: photo ? photo : "NULL",
      hire_date: hire_date ? hire_date : null,
      has_password: has_password ? has_password : null,
      password: has_password === 1 ? await bcrypt.hash(password, 12) : "NULL",
      two_factor: two_factor ? two_factor : null,
      role_id: role_id ? role_id : null,
      shift_id: shift_id ? shift_id : null,
      job_id: job_id ? job_id : null,
      salary: salary ? salary : null,
      contract_file: contract_file ? contract_file : "NULL",
      cv_link: cv_link ? cv_link : "NULL",
    },
    options: {
      getRequest: false,
    },
  };
  let token;
  const result = await run(myQuery);
  if (result?.registered_employee_id && has_password == 1) {
    token = await jwt.sign({ email, first_name, last_name }, JWT_SECRET_KEY, {
      expiresIn: "1h",
    });
  }
  return res.json({ ...result, token });
};

module.exports.update = async (req, res) => {
  const {
    user_id,
    country,
    city,
    district,
    home_number,
    map_location,
    phone,
    email,
    first_name,
    last_name,
    ssn,
    photo,
    hire_date,
    has_password,
    password,
    two_factor,
    role_id,
    shift_id,
    job_id,
    salary,
    contract_file,
    cv_link,
  } = req.body;
  const { employee_id } = req.params;
  const myQuery = {
    query: `EMPLOYEE_PACKAGE.UPDATE_EMPLOYEE(:user_id, :employee_id, :country, :city, :district, :home_number, :map_location, :phone, :email, :first_name, :last_name, :ssn, :photo, :hire_date, :has_password, :password, :two_factor, :role_id, :shift_id, :job_id, :salary, :contract_file, :cv_link)`,
    parameters: {
      user_id: user_id ? user_id : null,
      employee_id: employee_id ? employee_id : null,
      country: country ? country : "NULL",
      city: city ? city : "NULL",
      district: district ? district : "NULL",
      home_number: home_number ? home_number : null,
      map_location: map_location ? map_location : "NULL",
      phone: phone ? phone : "NULL",
      email: email ? email : null,
      first_name: first_name ? first_name : "NULL",
      last_name: last_name ? last_name : "NULL",
      ssn: ssn ? ssn : null,
      photo: photo ? photo : "NULL",
      hire_date: hire_date ? hire_date : null,
      has_password: has_password ? has_password : null,
      password: has_password === 1 ? await bcrypt.hash(password, 12) : "NULL",
      two_factor: two_factor ? two_factor : null,
      role_id: role_id ? role_id : null,
      shift_id: shift_id ? shift_id : null,
      job_id: job_id ? job_id : null,
      salary: salary ? salary : null,
      contract_file: contract_file ? contract_file : "NULL",
      cv_link: cv_link ? cv_link : "NULL",
    },
    options: {
      getRequest: false,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};

module.exports.remove = async (req, res) => {
  const { user_id } = req.body;
  const { employee_id } = req.params;
  const myQuery = {
    query: `EMPLOYEE_PACKAGE.DELETE_EMPLOYEE(:user_id, :employee_id)`,
    parameters: {
      user_id,
      employee_id,
    },
    options: {
      getRequest: false,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};

module.exports.getLimited = async (req, res) => {
  const { user_id } = req.params;
  const myQuery = {
    query: `EMPLOYEE_PACKAGE.GET_EMPLOYEES(:user_id, :return_cursor)`,
    parameters: {
      user_id,
    },
    options: {
      getRequest: true,
      rowCount: 2,
    },
  };
  const result = await run(myQuery);
  return res.json(result);
};
