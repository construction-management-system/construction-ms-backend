-- START of our project SEQUENCES
create sequence customer_sequence;
create sequence machinary_sequence;
CREATE SEQUENCE equipment_sequence;
-- END of our project SEQUENCES....


-- START of our project TYPES 
CREATE OR REPLACE TYPE increamental_array IS TABLE OF VARCHAR2(512);
-- END of our project TYPES

--  www.constructionMs/api/employee/registration
--  post --> [name, email, address],
--  success respose  --> {employee_id : 20}
--  error response --> { name_attr : 'error info', name_attr : 'error info'}

---------------- START of  validation PACKAGE ----------------------------------

-- START of PACKAGE Specification
CREATE OR REPLACE PACKAGE validation_package AS 
--  TYPE DECLARETION
    TYPE increamental_object IS TABLE OF VARCHAR2(512) INDEX BY VARCHAR2(512);

--  START of data_validation function declaretion
    FUNCTION data_validation(
        variable_array IN increamental_array,
        variable_name_array IN increamental_array,
        regex_array IN increamental_array,
        error_info_array IN increamental_array
    ) RETURN increamental_object;
--  END of data_validation function declaretion
END validation_package;
--  END of PACKAGE Specification

-- START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY validation_package AS 
-- START of input validation function
    FUNCTION data_validation(
        variable_array IN increamental_array,
        variable_name_array IN increamental_array,
        regex_array IN increamental_array,
        error_info_array IN increamental_array
    ) RETURN increamental_object IS
    
    error_object increamental_object;
    BEGIN
        FOR el in 1..regex_array.COUNT LOOP
            IF  NOT REGEXP_LIKE(variable_array(el), regex_array(el)) THEN
                error_object(variable_name_array(el)) := error_info_array(el);
            END IF;
        END LOOP;
          
        RETURN error_object;
    END;
--  END of input validation function
END validation_package; 
-- END of PACKAGE BODY
--------------------- END of validation PACKAGE---------------------------------

--------------------- START of  machinary TABLE or PACKAGE -----------------------

CREATE OR REPLACE PACKAGE machinary_package AS 
--  START of machinary registration function declaretion
    FUNCTION machinary_registration(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        MCH_NAME in MACHINARY.NAME%TYPE, 
        MCH_MODEL in MACHINARY.MODEL%TYPE, 
        MCH_PRICE IN MACHINARY.PRICE%TYPE,
        MCH_REGISTRATION_DATE IN MACHINARY.REGISTERATION_DATE%TYPE,
        MCH_DOCUMENTS IN MACHINARY.DOCUMENTS%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
    
--  END of machinary registration function declaretion

--  START of update machinary function declaretion
    FUNCTION update_machinary(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        MCH_ID in MACHINARY.MACHINARY_ID%TYPE,
        MCH_NAME in MACHINARY.NAME%TYPE, 
        MCH_MODEL in MACHINARY.MODEL%TYPE, 
        MCH_PRICE IN MACHINARY.PRICE%TYPE,
        MCH_REGISTRATION_DATE IN MACHINARY.REGISTERATION_DATE%TYPE,
        MCH_DOCUMENTS IN MACHINARY.DOCUMENTS%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
--  END of update machinary function declaretion

-- START OF DELETE machinary FUNCTION DECLARETION

FUNCTION delete_machinary (
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        DMACHINARY_ID IN MACHINARY.MACHINARY_ID%TYPE
        ) RETURN VALIDATION_PACKAGE.increamental_object;
        
-- END OF DELETE machinary FUNCTION DECLARETION

END machinary_package;
/

-- START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY machinary_package AS 
--- START of machinary registration function initialization
    FUNCTION machinary_registration(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        MCH_NAME in MACHINARY.NAME%TYPE, 
        MCH_MODEL in MACHINARY.MODEL%TYPE, 
        MCH_PRICE IN MACHINARY.PRICE%TYPE,
        MCH_REGISTRATION_DATE IN MACHINARY.REGISTERATION_DATE%TYPE,
        MCH_DOCUMENTS IN MACHINARY.DOCUMENTS%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object IS 
    
    machinary_id NUMBER;
    
    user_role_id number;
    
    user_role_name nvarchar2(256);
    
    variable_array increamental_array;
    variable_name_array increamental_array;
    regex_array increamental_array;
    error_info_array increamental_array;
    
    success_object  VALIDATION_PACKAGE.increamental_object;
    error_object  VALIDATION_PACKAGE.increamental_object;
    validation_error EXCEPTION;
    
    BEGIN
    
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = USER_ID;
        Select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = user_role_id;
        
        IF user_role_name != 'admin' OR user_role_name != 'secretary' THEN
            error_object('ERROR') := 'you do not have the permission!';
            RAISE validation_error;
        END IF;
        
        variable_array := increamental_array(MCH_NAME, MCH_MODEL, MCH_PRICE, MCH_REGISTRATION_DATE, MCH_DOCUMENTS);
        variable_name_array := increamental_array('MCH_NAME', 'MCH_MODEL', 'MCH_PRICE', 'MCH_REGISTRATION_DATE', 'MCH_DOCUMENTS');
        regex_array := increamental_array(
                                           '^[[:alpha:][:space:]]{1,256}$',
                                           '^[[:alnum:][:space:]]{1,256}$',
                                           '^([[:digit:]]{0,})(\.[[:digit:]]{0,})?$',                       -- for price
                                           '^([[:digit:]]{1,2})\/([[:digit:]]{1,2})\/([[:digit:]]{1,4})$',  -- for date
                                           '^[[:alnum:][:punct:]]{1,1023}$'                                 -- for documents and files
                                           );
        error_info_array := increamental_array(
                                                'MACHINARY NAME ACCEPTS STRING VALUES',
                                                'MACHINARY MODEL ACCEPTS NUMARICS VALUES',
                                                'MACHINARY PRICE ACCEPTS DIGITS ',
                                                'MACHINARY REGISTRATION ACCEPT DATE FORMAT (MM/DD/YYYY)',
                                                'DOCUMENTS ERROR'
                                                );
        
        error_object := VALIDATION_PACKAGE.DATA_VALIDATION(variable_array, variable_name_array, regex_array, error_info_array);

        IF error_object.COUNT > 0 THEN
            RAISE validation_error;
        ELSE
            INSERT INTO MACHINARY 
            VALUES(machinary_sequence.NEXTVAL, machinary_registration.MCH_NAME, machinary_registration.MCH_MODEL,
                machinary_registration.MCH_PRICE, machinary_registration.MCH_REGISTRATION_DATE, machinary_registration.MCH_DOCUMENTS)
            RETURNING MACHINARY.MACHINARY_ID into  machinary_registration.machinary_id;
            COMMIT;
            success_object('machinary_id') := machinary_registration.machinary_id;
        END IF;
    RETURN success_object; 
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            error_object('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN
            error_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN
            error_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN error_object;
    END;
--- END of machinary registration function initialization  

--  START of update machinary function initialization
    FUNCTION update_machinary(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        MCH_ID IN MACHINARY.MACHINARY_ID%TYPE,
        MCH_NAME in MACHINARY.NAME%TYPE, 
        MCH_MODEL in MACHINARY.MODEL%TYPE, 
        MCH_PRICE IN MACHINARY.PRICE%TYPE,
        MCH_REGISTRATION_DATE IN MACHINARY.REGISTERATION_DATE%TYPE,
        MCH_DOCUMENTS IN MACHINARY.DOCUMENTS%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object IS  
    
    machinary_id NUMBER;
    
    user_role_id NUMBER;
    
    user_role_name NVARCHAR2(256);
    
    variable_array increamental_array;
    variable_name_array increamental_array;
    regex_array increamental_array;
    error_info_array increamental_array;
    
    success_object  VALIDATION_PACKAGE.increamental_object;
    error_object  VALIDATION_PACKAGE.increamental_object;
    validation_error EXCEPTION;
    BEGIN
    
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = USER_ID;
        Select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = user_role_id;
        
        IF user_role_name != 'admin' OR user_role_name != 'secretary' THEN
            error_object('ERROR') := 'you do not have the permission!';
            RAISE validation_error;
        END IF;
        
        
        variable_array := increamental_array(MCH_NAME, MCH_MODEL, MCH_PRICE, MCH_REGISTRATION_DATE, MCH_DOCUMENTS);
        variable_name_array := increamental_array('NAME', 'MODEL', 'PRICE', 'REGISTRATION_DATE', 'DOCUMENTS');
        regex_array := increamental_array(
                                           '^[[:alpha:][:space:]]{1,256}$',
                                           '^[[:alnum:][:space:]]{1,256}$',
                                           '^([[:digit:]]{0,})(\.[[:digit:]]{0,})?$',                       -- for price
                                           '^([[:digit:]]{1,2})\/([[:digit:]]{1,2})\/([[:digit:]]{1,4})$',  -- for date
                                           '^[[:alnum:][:punct:]]{1,1023}$'                                 -- for documents and files
                                           );
        error_info_array := increamental_array(
                                                'MACHINARY NAME ACCEPTS STRING VALUES',
                                                'MACHINARY MODEL ACCEPTS NUMARICS VALUES',
                                                'MACHINARY PRICE ACCEPTS DIGITS ',
                                                'MACHINARY REGISTRATION ACCEPT DATE FORMAT (MM/DD/YYYY)',
                                                'DOCUMENTS ERROR'
                                                );
        
        error_object := VALIDATION_PACKAGE.DATA_VALIDATION(variable_array, variable_name_array, regex_array, error_info_array);

        IF error_object.COUNT > 0 THEN
            RAISE validation_error;
        ELSE
            UPDATE MACHINARY SET
                MACHINARY.NAME = update_machinary.MCH_NAME,
                MACHINARY.MODEL = update_machinary.MCH_MODEL,
                MACHINARY.PRICE = update_machinary.MCH_PRICE,
                MACHINARY.REGISTERATION_DATE = update_machinary.MCH_REGISTRATION_DATE,
                MACHINARY.DOCUMENTS = update_machinary.MCH_DOCUMENTS
            
            WHERE MACHINARY.MACHINARY_ID =  update_machinary.MCH_ID;
            COMMIT;
            success_object('update_machinary_id') := update_machinary.MCH_ID;
        END IF;
    RETURN success_object; 
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            error_object('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN
            error_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN
            error_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN error_object;
    END;
--  END of update machinary function initialization
    
-- START OF DELETE machinary FUNCTION INITIALIZATION

FUNCTION delete_machinary (
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        DMACHINARY_ID IN MACHINARY.MACHINARY_ID%TYPE
    ) RETURN VALIDATION_PACKAGE.increamental_object IS
    
    machinary_id NUMBER;
    
    user_role_id NUMBER;
    
    user_role_name NVARCHAR2(256);
    
    success_object VALIDATION_PACKAGE.increamental_object;
    error_object VALIDATION_PACKAGE.increamental_object;
    validation_error EXCEPTION;
    
BEGIN

    SELECT EMPLOYEE.ROLE_ID INTO user_role_id FROM EMPLOYEE WHERE EMPLOYEE.EMPLOYEE_ID = USER_ID;
    SELECT ROLE.NAME INTO user_role_name FROM ROLE WHERE ROLE.ROLE_ID = user_role_id;
    
    IF user_role_name != 'admin' OR user_role_name != 'secretary' THEN 
        error_object ('ERROR') := 'You don not have permision';
        RAISE validation_error;
    ELSE 
        DELETE FROM MACHINARY
        WHERE MACHINARY.MACHINARY_ID = delete_machinary.DMACHINARY_ID;
        
    success_object ('delete_machinary_id') := delete_machinary.DMACHINARY_ID;
    COMMIT;
    END IF;
    
    RETURN success_object;
    
EXCEPTION 
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found';
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := 'Some thing is wrong';
            RETURN error_object;
END;

-- END OF DELETE machinary FUNCTION INITIALIZATION 

END machinary_package; 
-- END of PACKAGE BODY
--------------------- END of machinary TABLE or PACKAGE--------------------------


--------------------- START of  equipment TABLE or PACKAGE -----------------------
CREATE OR REPLACE PACKAGE equipment_package AS 
--  START of equipment registration function declaretion
    FUNCTION equipment_registration(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        EQUP_NAME in EQUIPMENT.NAME%TYPE, 
        EQUP_DESCRIPTION IN EQUIPMENT.DESCRIPTION%TYPE, 
        EQUP_REGISTRATION_DATE IN EQUIPMENT.REGISRETION_DATE%TYPE,
        EQUP_PRICE IN EQUIPMENT.PRICE%TYPE,
        EQUP_COUNT_NUMBER IN EQUIPMENT.COUNT_NUMBER%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
    
--  END of equipment registration function declaretion

--  START of update equipment function declaretion
    FUNCTION update_equipment(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        EQUP_ID in EQUIPMENT.EQUIPMENT_ID%TYPE,
        EQUP_NAME in EQUIPMENT.NAME%TYPE, 
        EQUP_DESCRIPTION IN EQUIPMENT.DESCRIPTION%TYPE, 
        EQUP_REGISTRATION_DATE IN EQUIPMENT.REGISRETION_DATE%TYPE,
        EQUP_PRICE IN EQUIPMENT.PRICE%TYPE,
        EQUP_COUNT_NUMBER IN EQUIPMENT.COUNT_NUMBER%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
    
--  END of update equipment function declaretion

--  START of delete equipment function declaretion
    FUNCTION delete_equipment(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        DEQUIPMENT_ID IN EQUIPMENT.EQUIPMENT_ID%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
    
--  END of delete equipment function declaretion

END equipment_package;
/
-- START of EQUIPMENT PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY equipment_package AS 
--- START of equipment registration function initialization
    FUNCTION equipment_registration(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        EQUP_NAME in EQUIPMENT.NAME%TYPE, 
        EQUP_DESCRIPTION IN EQUIPMENT.DESCRIPTION%TYPE, 
        EQUP_REGISTRATION_DATE IN EQUIPMENT.REGISRETION_DATE%TYPE,
        EQUP_PRICE IN EQUIPMENT.PRICE%TYPE,
        EQUP_COUNT_NUMBER IN EQUIPMENT.COUNT_NUMBER%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object IS 
    
    equipment_id NUMBER;
    
    user_role_id NUMBER;
    
    user_role_name NVARCHAR2(256);
    
    variable_array increamental_array;
    variable_name_array increamental_array;
    regex_array increamental_array;
    error_info_array increamental_array;
    
    success_object  VALIDATION_PACKAGE.increamental_object;
    error_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
    
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = USER_ID;
        Select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = user_role_id;
        
        IF user_role_name != 'admin' OR user_role_name != 'secretary' THEN
            error_object('ERROR') := 'you do not have the permission!';
            RAISE validation_error;
        END IF;
        
        variable_array := increamental_array(EQUP_NAME, EQUP_DESCRIPTION, EQUP_REGISTRATION_DATE, EQUP_PRICE, EQUP_COUNT_NUMBER);
        variable_name_array := increamental_array('NAME', 'DESCRIPTION', 'REGISTRATION_DATE', 'PRICE', 'COUNT_NUMBER');
        regex_array := increamental_array(
                                           '^[[:alpha:][:space:]]{1,256}$',
                                           '^[[:alpha:][:space:]]{1,256}$',
                                           '^([[:digit:]]{1,2})\/([[:digit:]]{1,2})\/([[:digit:]]{1,4})$',  -- for date
                                           '^([[:digit:]]{0,})(\.[[:digit:]]{0,})?$',                       -- for price
                                           '^[[:digit:]]{0,}$'
                                           );
        error_info_array := increamental_array(
                                                'EQUIPMENT NAME ACCEPTS STRING VALUES',
                                                'EQUIPMENT DESCRIPTION ACCEPTS STRING VALUES',
                                                'EQUIPMENT REGISTRATION ACCEPT DATE FORMAT (MM/DD/YYYY)',
                                                'MACHINARY PRICE ACCEPTS DIGITS ',
                                                'COUNT NUMBER ERROR'
                                                );
        
        error_object := VALIDATION_PACKAGE.DATA_VALIDATION(variable_array, variable_name_array, regex_array, error_info_array);

        IF error_object.COUNT > 0 THEN
            RAISE validation_error;
        ELSE
            INSERT INTO EQUIPMENT 
            VALUES(equipment_sequence.NEXTVAL, equipment_registration.EQUP_NAME, 
            equipment_registration.EQUP_DESCRIPTION, equipment_registration.EQUP_REGISTRATION_DATE, 
            equipment_registration.EQUP_PRICE, equipment_registration.EQUP_COUNT_NUMBER)
            RETURNING EQUIPMENT.EQUIPMENT_ID into  equipment_registration.equipment_id;
            COMMIT;
            success_object('equipment_id') := equipment_registration.equipment_id;
        END IF;
    RETURN success_object; 
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            error_object('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN
            error_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN
            error_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN error_object;
    END;
--- END of equipment registration function initialization 

 --  START of update equipment function initialization
    FUNCTION update_equipment(
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        EQUP_ID IN EQUIPMENT.EQUIPMENT_ID%TYPE,
        EQUP_NAME IN EQUIPMENT.NAME%TYPE, 
        EQUP_DESCRIPTION IN EQUIPMENT.DESCRIPTION%TYPE, 
        EQUP_REGISTRATION_DATE IN EQUIPMENT.REGISRETION_DATE%TYPE,
        EQUP_PRICE IN EQUIPMENT.PRICE%TYPE,
        EQUP_COUNT_NUMBER IN EQUIPMENT.COUNT_NUMBER%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object IS  
    
    equipment_id NUMBER;
    
    user_role_id NUMBER;
    
    user_role_name NVARCHAR2(256);
    
    variable_array increamental_array;
    variable_name_array increamental_array;
    regex_array increamental_array;
    error_info_array increamental_array;
    
    success_object  VALIDATION_PACKAGE.increamental_object;
    error_object  VALIDATION_PACKAGE.increamental_object;
    validation_error EXCEPTION;
    BEGIN
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = USER_ID;
        Select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = user_role_id;
        
        IF user_role_name != 'admin' OR user_role_name != 'secretary' THEN
            error_object('ERROR') := 'you do not have the permission!';
            RAISE validation_error;
        END IF;
        
        variable_array := increamental_array(EQUP_NAME, EQUP_DESCRIPTION, EQUP_REGISTRATION_DATE, EQUP_PRICE, EQUP_COUNT_NUMBER);
        variable_name_array := increamental_array('NAME', 'DESCRIPTION', 'REGISTRATION_DATE', 'PRICE', 'COUNT_NUMBER');
        regex_array := increamental_array(
                                           '^[[:alpha:][:space:]]{1,256}$',
                                           '^[[:alpha:][:space:]]{1,256}$',
                                           '^([[:digit:]]{1,2})\/([[:digit:]]{1,2})\/([[:digit:]]{1,4})$',  -- for date
                                           '^([[:digit:]]{0,})(\.[[:digit:]]{0,})?$',                       -- for price
                                           '^[[:digit:]]{0,}$'
                                           );
        error_info_array := increamental_array(
                                                'EQUIPMENT NAME ACCEPTS STRING VALUES',
                                                'EQUIPMENT DESCRIPTION ACCEPTS STRING VALUES',
                                                'EQUIPMENT REGISTRATION ACCEPT DATE FORMAT (MM/DD/YYYY)',
                                                'MACHINARY PRICE ACCEPTS DIGITS ',
                                                'COUNT NUMBER ERROR'
                                                );
        
        error_object := VALIDATION_PACKAGE.DATA_VALIDATION(variable_array, variable_name_array, regex_array, error_info_array);

        IF error_object.COUNT > 0 THEN
            RAISE validation_error;
        ELSE
            UPDATE EQUIPMENT SET
                EQUIPMENT.NAME = update_equipment.EQUP_NAME,
                EQUIPMENT.DESCRIPTION = update_equipment.EQUP_DESCRIPTION,
                EQUIPMENT.REGISRETION_DATE = update_equipment.EQUP_REGISTRATION_DATE,
                EQUIPMENT.PRICE = update_equipment.EQUP_PRICE,
                EQUIPMENT.COUNT_NUMBER = update_equipment.EQUP_COUNT_NUMBER
            WHERE EQUIPMENT.EQUIPMENT_ID = update_equipment.equipment_id;
            COMMIT;
            success_object('update_equipment_id') := update_equipment.equipment_id;
        END IF;
    RETURN success_object; 
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            error_object('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN
            error_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN
            error_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN error_object;
    END;
    
--- END of equipment update function initialization 
    
 --  START of delete equipment function initialization
 
 FUNCTION delete_equipment (
        USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        DEQUIPMENT_ID IN EQUIPMENT.EQUIPMENT_ID%TYPE
    ) RETURN VALIDATION_PACKAGE.increamental_object IS

    equipment_id NUMBER;
    
    user_role_id NUMBER;
    
    user_role_name NVARCHAR2(256);
    
    success_object VALIDATION_PACKAGE.increamental_object;
    error_object VALIDATION_PACKAGE.increamental_object;
    validatio_error EXCEPTION;
    
BEGIN
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = USER_ID;
        Select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = user_role_id;
        
        IF user_role_name != 'admin' OR user_role_name != 'secretary' THEN
            error_object('ERROR') := 'you do not have the permission';
            RAISE validation_error;
            
        ELSE 
            DELETE FROM EQUIPMENT
            WHERE EQUIPMENT.EQUIPMENT_ID = delete_equipment.DEQUIPMENT_ID;
    
            success_object ('equipment_deleted') := 'succeed';
            COMMIT;
        END IF;
    
    RETURN success_object;
EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found';
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := 'Some thing is wrong';
            RETURN error_object;
END;

-- END of delete equipment function initialization

END equipment_package; 
-- END of PACKAGE BODY

--------------------- END of equipment TABLE or PACKAGE--------------------------

--------------------- START of  customer TABLE or PACKAGE -----------------------

CREATE OR REPLACE package customer_package As 

--  START of customer data checking function declaretion

FUNCTION customer_data_validation (
        FIRST_NAME in CUSTOMER.FIRST_NAME%type,
        LAST_NAME in CUSTOMER.LAST_NAME%type,
        SSN in CUSTOMER.SSN%type,
        PHOTO in CUSTOMER.PHOTO%type,
        REGISTRATION_DATE in CUSTOMER.REGISTRATION_DATE%type
    ) return VALIDATION_PACKAGE.increamental_object;
    
    --  END of customer data checking function declaretion
    
    --  START of customer registration function declaretion
    FUNCTION register_customer(
        R_user_id in EMPLOYEE.EMPLOYEE_ID%TYPE,
        R_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        R_CITY in ADDRESS.CITY%TYPE,
        R_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        R_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        R_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        R_PHONE in CONTACT.PHONE%TYPE, 
        R_EMAIL in CONTACT.EMAIL%TYPE, 
        R_FIRST_NAME in CUSTOMER.FIRST_NAME%type,
        R_LAST_NAME in CUSTOMER.LAST_NAME%type,
        R_SSN in CUSTOMER.SSN%type,
        R_PHOTO in CUSTOMER.PHOTO%type,
        R_REGISTRATION_DATE in CUSTOMER.REGISTRATION_DATE%type
    ) return VALIDATION_PACKAGE.increamental_object;
    
    --  END of customer registration function declaretion
    
    --  START of update customer function declaretion
    FUNCTION update_customer(
        U_user_id in EMPLOYEE.EMPLOYEE_ID%TYPE,
        U_CUSTOMER_ID IN CUSTOMER.CUSTOMER_ID%TYPE,
        U_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        U_CITY in ADDRESS.CITY%TYPE,
        U_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        U_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        U_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        U_PHONE in CONTACT.PHONE%TYPE, 
        U_EMAIL in CONTACT.EMAIL%TYPE, 
        U_FIRST_NAME in CUSTOMER.FIRST_NAME%type,
        U_LAST_NAME in CUSTOMER.LAST_NAME%type,
        U_SSN in CUSTOMER.SSN%type,
        U_PHOTO in CUSTOMER.PHOTO%type,
        U_REGISTRATION_DATE in CUSTOMER.REGISTRATION_DATE%type
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
    
--  END of update customer function declaretion

--  START of delete customer function declaretion

FUNCTION delete_customer (
        D_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        D_CUSTOMER_ID IN CUSTOMER.CUSTOMER_ID%TYPE
    ) RETURN VALIDATION_PACKAGE.increamental_object;

--  END of delete customer function declaretion

END customer_package;
-- END OF PACKAGE SPECIFICATION ...
/

-- START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY customer_package AS 
--- START of customer data validation  function initialization
    FUNCTION customer_data_validation(
        FIRST_NAME in CUSTOMER.FIRST_NAME%type,
        LAST_NAME in CUSTOMER.LAST_NAME%type,
        SSN in CUSTOMER.SSN%type,
        PHOTO in CUSTOMER.PHOTO%type,
        REGISTRATION_DATE in CUSTOMER.REGISTRATION_DATE%type
    ) RETURN  VALIDATION_PACKAGE.increamental_object IS 
    
    variable_array increamental_array;
    variable_name_array increamental_array;
    regex_array increamental_array;
    error_info_array increamental_array;
    
    success_object  VALIDATION_PACKAGE.increamental_object;
    error_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
        variable_array := increamental_array(FIRST_NAME, LAST_NAME, SSN, PHOTO, REGISTRATION_DATE);
        variable_name_array := increamental_array('FIRST_NAME', 'LAST_NAME', 'SSN', 'PHOTO', 'REGISTRATION_DATE');
        regex_array := increamental_array(
                                    '^[[:alpha:][:space:]]{1,256}$',
                                    '^[[:alpha:][:space:]]{1,256}$',    -- for string
                                    '^[[:digit:]]{0,}$',                -- 
                                    '^[[:alnum:][:punct:]]{1,1023}$',   -- for photos 
                                    '^([[:digit:]]{1,2})\/([[:digit:]]{1,2})\/([[:digit:]]{1,4})$' -- for date
                        );
        error_info_array := increamental_array(
                                    'FIRST NAME ERROR',
                                    'LAST NAME ERROR',
                                    'SSN ERROR',
                                    'PHOTO ERROR',
                                    'REGISTRATION DATE ERROR'
                                    );
        error_object := VALIDATION_PACKAGE.DATA_VALIDATION(variable_array, variable_name_array, regex_array, error_info_array);
        
        IF error_object.COUNT > 0 THEN
            RAISE validation_error;
        ELSE
            success_object('customer_data_validated') := 'succeed';
        END IF;
        
        RETURN success_object; 
        EXCEPTION 
        WHEN validation_error THEN
            RETURN error_object;
        END;
        --- END of customer data validating function initialization  
        
        --  START of customer registration function initialization
        FUNCTION register_customer(
        R_user_id in EMPLOYEE.EMPLOYEE_ID%TYPE,
        R_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        R_CITY in ADDRESS.CITY%TYPE,
        R_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        R_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        R_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        R_PHONE in CONTACT.PHONE%TYPE, 
        R_EMAIL in CONTACT.EMAIL%TYPE, 
        R_FIRST_NAME in CUSTOMER.FIRST_NAME%type,
        R_LAST_NAME in CUSTOMER.LAST_NAME%type,
        R_SSN in CUSTOMER.SSN%type,
        R_PHOTO in CUSTOMER.PHOTO%type,
        R_REGISTRATION_DATE in CUSTOMER.REGISTRATION_DATE%type
        ) RETURN VALIDATION_PACKAGE.increamental_object IS
        
    contact_id NUMBER;
    customer_id NUMBER;
    address_id NUMBER;
    
    user_role_id number;
    
    user_role_name nvarchar2(256);

    success_object  VALIDATION_PACKAGE.increamental_object;
    response_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = R_user_id;
        Select ROLE.NAME into user_role_name from role where role.ROLE_ID = user_role_id;
        
    If user_role_name = 'admin' or user_role_name = 'secretary' THEN
    
        response_object := ADDRESS_PACKAGE.REGISTER_ADDRESS(register_customer.R_COUNTRY, register_customer.R_CITY, register_customer.R_DISTRICT, register_customer.R_HOME_NUMBER, register_customer.R_MAP_LOCATION);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_address_id' THEN
            RAISE validation_error;
        ELSE
            register_customer.address_id := response_object(response_object.FIRST);
        END IF;
        
        response_object := CONTACT_PACKAGE.REGISTER_CONTACT(register_customer.R_PHONE, register_customer.R_EMAIL, register_customer.address_id);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_contact_id' THEN
            RAISE validation_error;
        ELSE
            register_customer.contact_id := response_object(response_object.FIRST);
        END IF;
    
    response_object := CUSTOMER_PACKAGE.CUSTOMER_DATA_VALIDATION(register_customer.R_FIRST_NAME, register_customer.R_LAST_NAME,
        register_customer.R_SSN, register_customer.R_PHOTO, register_customer.R_REGISTRATION_DATE);
    IF response_object.COUNT > 0 AND response_object(response_object.FIRST) != 'succeed' THEN
        RAISE validation_error;
    ELSE
        INSERT INTO CUSTOMER
        VALUES (customer_sequence.NEXTVAL, register_customer.R_FIRST_NAME, register_customer.R_LAST_NAME, 
        register_customer.R_SSN, register_customer.CONTACT_ID, register_customer.R_PHOTO, register_customer.R_REGISTRATION_DATE)
        RETURNING CUSTOMER.CUSTOMER_ID INTO register_customer.customer_id;
        success_object('register_customer_id') := register_customer.customer_id;
        COMMIT;
    END IF;
  
    ELSE
        response_object('ERROR') := 'You do not have permission';
        RAISE validation_error;
    END IF;
        return success_object;
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            response_object('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN
            response_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN response_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN
            response_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN response_object;
    END;
--  END of customer data checking function initialization


--  START of update customer  function initialization

    FUNCTION update_customer(
        U_user_id in EMPLOYEE.EMPLOYEE_ID%TYPE,
        U_CUSTOMER_ID IN CUSTOMER.CUSTOMER_ID%TYPE,
        U_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        U_CITY in ADDRESS.CITY%TYPE,
        U_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        U_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        U_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        U_PHONE in CONTACT.PHONE%TYPE, 
        U_EMAIL in CONTACT.EMAIL%TYPE, 
        U_FIRST_NAME in CUSTOMER.FIRST_NAME%type,
        U_LAST_NAME in CUSTOMER.LAST_NAME%type,
        U_SSN in CUSTOMER.SSN%type,
        U_PHOTO in CUSTOMER.PHOTO%type,
        U_REGISTRATION_DATE in CUSTOMER.REGISTRATION_DATE%type
        ) RETURN VALIDATION_PACKAGE.increamental_object IS
        
    contact_id NUMBER;
    customer_id NUMBER;
    address_id NUMBER;
    
    user_role_id number;
    
    user_role_name nvarchar2(256);

    success_object  VALIDATION_PACKAGE.increamental_object;
    response_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = U_user_id;
        Select ROLE.NAME into user_role_name from role where role.ROLE_ID = user_role_id;
        
    IF update_customer.user_role_name = 'admin' OR update_customer.user_role_name = 'secretary' THEN
        select CUSTOMER.CONTACT_ID into update_customer.contact_id from CUSTOMER where CUSTOMER.CUSTOMER_ID = update_customer.U_CUSTOMER_ID;            
        select CONTACT.ADDRESS_ID into update_customer.address_id from CONTACT where CONTACT.CONTACT_ID = update_customer.contact_id;
        
        response_object := ADDRESS_PACKAGE.UPDATE_ADDRESS(update_customer.address_id, update_customer.U_COUNTRY, update_customer.U_CITY, update_customer.U_DISTRICT, update_customer.U_HOME_NUMBER, update_customer.U_MAP_LOCATION);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'updated_address_id' THEN
            RAISE validation_error;
        END IF;
        
        response_object := CONTACT_PACKAGE.UPDATE_CONTACT(update_customer.contact_id, update_customer.U_PHONE, update_customer.U_EMAIL, update_customer.address_id);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'updated_contact_id' THEN
            RAISE validation_error;
        END IF;  
    
    
    response_object := CUSTOMER_PACKAGE.CUSTOMER_DATA_VALIDATION(update_customer.U_FIRST_NAME, update_customer.U_LAST_NAME,
        update_customer.U_SSN, update_customer.U_PHOTO, update_customer.U_REGISTRATION_DATE);
    IF response_object.COUNT > 0 AND response_object(response_object.FIRST) != 'succeed' THEN
        RAISE validation_error;
    ELSE
        UPDATE CUSTOMER SET
        CUSTOMER.FIRST_NAME         = update_customer.U_FIRST_NAME,
        CUSTOMER.LAST_NAME          = update_customer.U_LAST_NAME,
        CUSTOMER.SSN                = update_customer.U_SSN,
        CUSTOMER.CONTACT_ID         = update_customer.CONTACT_ID,
        CUSTOMER.PHOTO              = update_customer.U_PHOTO,
        CUSTOMER.REGISTRATION_DATE  = update_customer.U_REGISTRATION_DATE
        WHERE CUSTOMER.CUSTOMER_ID  = update_customer.U_customer_id;
        success_object('customer_updated') := 'succeed';
        COMMIT;
    END IF;
    
    ELSE
        response_object('ERROR') := 'You do not update permission';
        RAISE validation_error;
    END IF;
        return success_object;
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            response_object('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN
            response_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN response_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN
            response_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN response_object;
    END;
    
--  END of update customer  function initialization

--  START of delete customer  function initialization

FUNCTION delete_customer (
        D_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        D_CUSTOMER_ID IN CUSTOMER.CUSTOMER_ID%TYPE
    ) RETURN VALIDATION_PACKAGE.increamental_object IS
    
    customer_id NUMBER;
    
    user_role_id number;
    
    user_role_name nvarchar2(256);
    
    success_object VALIDATION_PACKAGE.increamental_object;
    response_object VALIDATION_PACKAGE.increamental_object;
    
validation_error EXCEPTION;
BEGIN
    SELECT EMPLOYEE.ROLE_ID INTO delete_customer.user_role_id FROM EMPLOYEE 
    WHERE EMPLOYEE.EMPLOYEE_ID = delete_customer.D_USER_ID;
    Select ROLE.NAME into user_role_name from role where role.ROLE_ID = user_role_id;
    
IF delete_customer.user_role_name = 'admin' THEN
    
    DELETE FROM CUSTOMER 
    WHERE CUSTOMER.CUSTOMER_ID = delete_customer.D_CUSTOMER_ID;
    
    success_object ('customer_deleted') := 'succeed';
    
    COMMIT;
        ELSE response_object ('ERROR') := 'You do not have permission';

    RAISE validation_error;

    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN ROLLBACK;

        response_object ('ERROR') := 'Too many records returned';

    RETURN response_object;

        WHEN NO_DATA_FOUND THEN response_object ('ERROR') := 'No record found';

        ROLLBACK;

    RETURN response_object;

        WHEN OTHERS THEN response_object ('ERROR') := 'Some thing is wrong';

        ROLLBACK;

    RETURN response_object;
    
END;

--  END of delete customer function initialization

END customer_package; 
-- END of PACKAGE BODY
--------------------- END of customer TABLE or PACKAGE--------------------------

