/* Formatted on 2/8/2021 3:12:54 PM (QP5 v5.336) */

-- START of our project SEQUENCES
/* 
create sequence employee_sequence;
create sequence auth_sequence;
create sequence contact_sequence;
CREATE SEQUENCE address_sequence;
create sequence role_sequence;
create sequence shift_sequence;
create sequence job_sequence;
*/
-- END of our project SEQUENCES

-- START of our project TYPES 
CREATE OR REPLACE TYPE increamental_array IS TABLE OF VARCHAR2(512);
-- END of our project TYPES


---------------- START of  validation PACKAGE ----------------------------------
-- START of PACKAGE Specification
CREATE OR REPLACE PACKAGE validation_package AS 
--  TYPE DECLARETION
TYPE increamental_object IS TABLE OF VARCHAR2 (512) INDEX BY VARCHAR2 (512);

--  START of data_validation function declaretion
FUNCTION data_validation (
    variable_array IN increamental_array,
    variable_name_array IN increamental_array,
    regex_array IN increamental_array,
    error_info_array IN increamental_array
) RETURN increamental_object;

--  END of data_validation function declaretion
END validation_package;

--  END of PACKAGE Specification
-- START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY validation_package AS 
-- START of input validation function
FUNCTION data_validation (
    variable_array IN increamental_array,
    variable_name_array IN increamental_array,
    regex_array IN increamental_array,
    error_info_array IN increamental_array
) RETURN increamental_object IS error_object increamental_object;

BEGIN 
    FOR el IN 1..regex_array.COUNT LOOP 
        IF NOT REGEXP_LIKE (variable_array (el), regex_array (el)) THEN 
            error_object (variable_name_array (el)) := error_info_array (el);
        END IF;
    END LOOP;

    RETURN error_object;

END;

--  END of input validation function
END validation_package;

-- END of PACKAGE BODY
--------------------- END of validation PACKAGE---------------------------------


--------------------- START of  address TABLE or PACKAGE -----------------------
-- START of PACKAGE Specification
CREATE OR REPLACE PACKAGE address_package AS 
--  START of register address function declaretion
FUNCTION register_address (
    COUNTRY IN ADDRESS.COUNTRY % TYPE,
    CITY IN ADDRESS.CITY % TYPE,
    DISTRICT IN ADDRESS.DISTRICT % TYPE,
    HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;

--  END of register address function declaretion

--  START of update address function declaretion
FUNCTION update_address (
    ADDRESS_ID IN ADDRESS.ADDRESS_ID % TYPE,
    COUNTRY IN ADDRESS.COUNTRY % TYPE,
    CITY IN ADDRESS.CITY % TYPE,
    DISTRICT IN ADDRESS.DISTRICT % TYPE,
    HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;

--  END of update address function declaretion
--  START of delete address function declaretion
FUNCTION delete_address (
    ADDRESS_ID IN ADDRESS.ADDRESS_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;

--  END of delete address function declaretion
END address_package;

--  END of PACKAGE Specification
-- START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY address_package AS 

--- START of register address function initialization
FUNCTION register_address (
    COUNTRY IN ADDRESS.COUNTRY % TYPE,
    CITY IN ADDRESS.CITY % TYPE,
    DISTRICT IN ADDRESS.DISTRICT % TYPE,
    HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

address_id NUMBER;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN 
    variable_array := increamental_array (
        COUNTRY,
        CITY,
        DISTRICT,
        HOME_NUMBER,
        MAP_LOCATION
    );

    variable_name_array := increamental_array (
        'COUNTRY',
        'CITY',
        'DISTRICT',
        'HOME_NUMBER',
        'MAP_LOCATION'
    );

    regex_array := increamental_array (
        '(^[[:alpha:][:space:]]{1,256}$)|(NULL)|(null)',
        '(^[[:alpha:][:space:]]{1,256}$)|(NULL)|(null)',
        '(^[[:alnum:][:space:]]{1,256}$)|(NULL)|(null)',
        '(^[[:digit:]]{0,11}$)|(NULL)|(null)',
        '(^(([[:digit:]]{1,5})\.([[:digit:]]{1,5})\.([[:digit:]]{1,5}))[[:space:]]*\,[[:space:]]*(([[:digit:]]{1,5})\.([[:digit:]]{1,5})\.([[:digit:]]{1,5}))$)|(NULL)|(null)'
    );

    error_info_array := increamental_array (
        'country attribute can only accept  string and it should be between 1 and 256 range.',
        'city attribute can only accept  string and it should be between 1 and 256 range.',
        'District attribute can only accept  alphanumric and it should be between 1 and 256 range.',
        'home number attribute can only accept digits and it can take 1 to 11 digits .',
        'map lucation attribute can only accept input in this format (digit.digit.digit , digit.digit.digit).'
    );

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        INSERT INTO
            address
        VALUES
            (
                address_sequence.NEXTVAL,
                register_address.COUNTRY,
                register_address.CITY,
                register_address.DISTRICT,
                register_address.HOME_NUMBER,
                register_address.MAP_LOCATION
            ) 
        RETURNING ADDRESS.ADDRESS_ID INTO register_address.address_id;

        success_object ('registered_address_id') := register_address.address_id;

    END IF;
    RETURN success_object;
    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned in register address';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in register address';
            RETURN error_object;
        WHEN validation_error THEN 
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in register address';
            RETURN error_object;
END;
--- END of register address function initialization

--  START of update address function initialization
FUNCTION update_address (
    ADDRESS_ID IN ADDRESS.ADDRESS_ID % TYPE,
    COUNTRY IN ADDRESS.COUNTRY % TYPE,
    CITY IN ADDRESS.CITY % TYPE,
    DISTRICT IN ADDRESS.DISTRICT % TYPE,
    HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

is_address_id number;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN 
    variable_array := increamental_array (
        COUNTRY,
        CITY,
        DISTRICT,
        HOME_NUMBER,
        MAP_LOCATION
    );

    variable_name_array := increamental_array (
        'COUNTRY',
        'CITY',
        'DISTRICT',
        'HOME_NUMBER',
        'MAP_LOCATION'
    );

regex_array := increamental_array (
        '(^[[:alpha:][:space:]]{1,256}$)|(NULL)|(null)',
        '(^[[:alpha:][:space:]]{1,256}$)|(NULL)|(null)',
        '(^[[:alnum:][:space:]]{1,256}$)|(NULL)|(null)',
        '(^[[:digit:]]{0,11}$)|(NULL)|(null)',
        '(^(([[:digit:]]{1,5})\.([[:digit:]]{1,5})\.([[:digit:]]{1,5}))[[:space:]]*\,[[:space:]]*(([[:digit:]]{1,5})\.([[:digit:]]{1,5})\.([[:digit:]]{1,5}))$)|(NULL)|(null)'
    );

    error_info_array := increamental_array (
        'country attribute can only accept  string and it should be between 1 and 256 range.',
        'city attribute can only accept  string and it should be between 1 and 256 range.',
        'District attribute can only accept  alphanumric and it should be between 1 and 256 range.',
        'home number attribute can only accept digits and it can take 1 to 11 digits .',
        'map lucation attribute can only accept input in this format (digit.digit.digit , digit.digit.digit).'
    );

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        UPDATE
            address
        SET
            address.COUNTRY = update_address.COUNTRY,
            address.CITY = update_address.CITY,
            address.DISTRICT = update_address.DISTRICT,
            address.HOME_NUMBER = update_address.HOME_NUMBER,
            address.MAP_LOCATION = update_address.MAP_LOCATION
        WHERE
            address.ADDRESS_ID = update_address.ADDRESS_ID
        RETURNING address.address_id into is_address_id;
        
        IF is_address_id is null THEN
            error_object('ERROR') := 'No record found';
            raise validation_error;
        END IF;
        success_object ('updated_address_id') := update_address.ADDRESS_ID;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned in update address';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in update address';
            RETURN error_object;
        WHEN validation_error THEN 
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in update address';
            RETURN error_object;
END;

--- END of update address function initialization
--  START of delete address function initialization
FUNCTION delete_address (
    ADDRESS_ID IN ADDRESS.ADDRESS_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object AS

is_address_id number;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

BEGIN
    DELETE FROM
        address
    WHERE
        address.address_id = delete_address.ADDRESS_ID;
        
    success_object ('deleted_address_id') := delete_address.ADDRESS_ID;
    RETURN success_object;
    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned in delete address';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in delete address';
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in delete address';
            RETURN error_object;
END;
--  END of delete address function initialization
END address_package;
-- END of PACKAGE BODY
--------------------- END of  address TABLE or PACKAGE--------------------------

--------------------- START of  contact TABLE or PACKAGE -----------------------
--  START of PACKAGE Specification
CREATE OR REPLACE PACKAGE contact_package AS 

--  START of register contact function declaretion
FUNCTION register_contact (
    R_COUNTRY IN ADDRESS.COUNTRY%TYPE,
    R_CITY IN ADDRESS.CITY%TYPE,
    R_DISTRICT IN ADDRESS.DISTRICT%TYPE,
    R_HOME_NUMBER IN ADDRESS.HOME_NUMBER%TYPE,
    R_MAP_LOCATION IN ADDRESS.MAP_LOCATION%TYPE,
    R_PHONE IN CONTACT.PHONE % TYPE,
    R_EMAIL IN CONTACT.EMAIL % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of register contact function declaretion

--  START of update contact function declaretion
FUNCTION update_contact (
    U_CONTACT_ID IN CONTACT.CONTACT_ID%TYPE,
    U_COUNTRY IN ADDRESS.COUNTRY%TYPE,
    U_CITY IN ADDRESS.CITY%TYPE,
    U_DISTRICT IN ADDRESS.DISTRICT%TYPE,
    U_HOME_NUMBER IN ADDRESS.HOME_NUMBER%TYPE,
    U_MAP_LOCATION IN ADDRESS.MAP_LOCATION%TYPE,
    U_PHONE IN CONTACT.PHONE % TYPE,
    U_EMAIL IN CONTACT.EMAIL % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of update contact function declaretion

--  START of delete contact function declaretion
FUNCTION delete_contact (CONTACT_ID IN CONTACT.CONTACT_ID % TYPE) 
    RETURN VALIDATION_PACKAGE.increamental_object;

--  END of delete contact function declaretion
END contact_package;

--  END of PACKAGE Specification

--  START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY contact_package AS 
--- START of register contact function initialization
FUNCTION register_contact (
    R_COUNTRY IN ADDRESS.COUNTRY%TYPE,
    R_CITY IN ADDRESS.CITY%TYPE,
    R_DISTRICT IN ADDRESS.DISTRICT%TYPE,
    R_HOME_NUMBER IN ADDRESS.HOME_NUMBER%TYPE,
    R_MAP_LOCATION IN ADDRESS.MAP_LOCATION%TYPE,
    R_PHONE IN CONTACT.PHONE % TYPE,
    R_EMAIL IN CONTACT.EMAIL % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

contact_id NUMBER;
address_id NUMBER;
is_email NUMBER;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN 

    error_object := ADDRESS_PACKAGE.REGISTER_ADDRESS (
            register_contact.R_COUNTRY,
            register_contact.R_CITY,
            register_contact.R_DISTRICT,
            register_contact.R_HOME_NUMBER,
            register_contact.R_MAP_LOCATION
        );

    IF error_object.COUNT > 0 AND error_object.FIRST != 'registered_address_id' THEN 
        RAISE validation_error;
    ELSE 
        register_contact.address_id := error_object (error_object.FIRST);
    END IF;
    variable_array := increamental_array (R_PHONE, R_EMAIL, register_contact.address_id);
    variable_name_array := increamental_array ('PHONE', 'EMAIL', 'ADDRESS_ID');
    regex_array := increamental_array (
        '^[[:digit:]]{10,13}$',
        '(^([^.^[:space:]][[:alnum:]\-\._]+)@([[:alnum:]-]+)\.([[:alpha:]]{2,8})([[:alnum:]]{2,8})?$)|(null)|(NULL)',
        '(^[[:digit:]]{1,}$)|(null)|(NULL)'
    );
    error_info_array := increamental_array (
        'phone number should be between 10 to 13 digits',
        'email must be valid',
        'address_id must be number'
    );

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        select (select contact_id from contact where contact.email = R_EMAIL) into is_email from dual;
        
        IF register_contact.is_email is not null THEN
            error_object('ERROR') := 'User already exist with the given email';
            RAISE validation_error;
        END IF; 
        INSERT INTO
            contact
        VALUES
            (
                contact_sequence.NEXTVAL,
                register_contact.R_PHONE,
                register_contact.R_EMAIL,
                register_contact.ADDRESS_ID
            ) RETURNING CONTACT.CONTACT_ID INTO register_contact.contact_id;

        success_object ('registered_contact_id') := register_contact.contact_id;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned in register contact';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in register contact';
            RETURN error_object;
        WHEN validation_error THEN 
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in register contact';
            RETURN error_object;
END;
--- END of register contact function initialization

--- START of update contact function initialization
FUNCTION update_contact (
    U_CONTACT_ID IN CONTACT.CONTACT_ID%TYPE,
    U_COUNTRY IN ADDRESS.COUNTRY%TYPE,
    U_CITY IN ADDRESS.CITY%TYPE,
    U_DISTRICT IN ADDRESS.DISTRICT%TYPE,
    U_HOME_NUMBER IN ADDRESS.HOME_NUMBER%TYPE,
    U_MAP_LOCATION IN ADDRESS.MAP_LOCATION%TYPE,
    U_PHONE IN CONTACT.PHONE % TYPE,
    U_EMAIL IN CONTACT.EMAIL % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS

address_id NUMBER;
is_contact_id number;
is_email number;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN 
        select (select address_id from contact where contact_id = U_CONTACT_ID) into address_id from dual;
        IF update_contact.address_id is not null THEN
                error_object := ADDRESS_PACKAGE.UPDATE_ADDRESS (
                update_contact.address_id,
                update_contact.U_COUNTRY,
                update_contact.U_CITY,
                update_contact.U_DISTRICT,
                update_contact.U_HOME_NUMBER,
                update_contact.U_MAP_LOCATION
            );
            IF error_object.COUNT > 0 AND error_object.FIRST != 'updated_address_id' THEN
                RAISE validation_error;
            END IF;
        ELSE
            error_object := ADDRESS_PACKAGE.REGISTER_ADDRESS(
                update_contact.U_COUNTRY,
                update_contact.U_CITY,
                update_contact.U_DISTRICT,
                update_contact.U_HOME_NUMBER,
                update_contact.U_MAP_LOCATION
            );
            IF error_object.COUNT > 0 AND error_object.FIRST != 'registered_address_id' THEN
                RAISE validation_error;
            END IF; 
            address_id := error_object(error_object.FIRST);
        END IF;


    variable_array := increamental_array (U_PHONE, U_EMAIL, ADDRESS_ID);
    variable_name_array := increamental_array ('PHONE', 'EMAIL', 'ADDRESS_ID');

    regex_array := increamental_array (
        '^[[:digit:]]{10,13}$',
        '(^([^.^[:space:]][[:alnum:]\-\._]+)@([[:alnum:]-]+)\.([[:alpha:]]{2,8})([[:alnum:]]{2,8})?$)|(null)|(NULL)',
        '(^[[:digit:]]{1,}$)|(null)|(NULL)'
    );

    error_info_array := increamental_array (
        'phone number should be between 10 to 13 digits',
        'email must be valid email',
        'address_id must be number'
    );

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );
    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
            UPDATE
                contact
            SET
                contact.PHONE = update_contact.U_PHONE,
                contact.EMAIL = update_contact.U_EMAIL,
                contact.ADDRESS_ID = update_contact.ADDRESS_ID
            WHERE
                contact.CONTACT_ID = update_contact.U_CONTACT_ID;
            success_object ('updated_contact_id') := update_contact.U_CONTACT_ID;        
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned in update contact';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in update contact';
            RETURN error_object;
        WHEN validation_error THEN 
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in update contact';
            RETURN error_object;
END;

--- END of update contact function initialization

--  START of delete contact function initialization
FUNCTION delete_contact (CONTACT_ID IN CONTACT.CONTACT_ID % TYPE) 
RETURN VALIDATION_PACKAGE.increamental_object AS 

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

BEGIN
    DELETE FROM
        contact
    WHERE
        contact.contact_id = delete_contact.CONTACT_ID;

    success_object ('deleted_contact_id') := delete_contact.CONTACT_ID;
    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            error_object ('ERROR') := 'Too many records returned IN DELETE CONTACT ';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in delete contact';
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in delete contact';
            RETURN error_object;
END;

--  END of delete address function initialization
END contact_package;

-- END of PACKAGE BODY
--------------------- END of  contact TABLE or PACKAGE--------------------------


--------------------- START of  role TABLE or PACKAGE --------------------------
--  START of PACKAGE Specification
CREATE OR REPLACE PACKAGE role_package AS 
--- START of register role function declaretion
FUNCTION register_role (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    ROLE_NAME IN ROLE.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--- END of register role function declaretion

--  START of update role function declaretion
FUNCTION update_role (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    ROLE_ID IN ROLE.ROLE_ID % TYPE,
    ROLE_NAME IN ROLE.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of update role  function declaretion

--  START of delete role function declaretion
FUNCTION delete_role (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    ROLE_ID IN ROLE.ROLE_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of delete role  function declaretion

--  START of get roles function declaretion
PROCEDURE get_roles (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
);
--  END of get roles function declaretion
END role_package;

--  END of PACKAGE Specification

--  START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY role_package AS 
--- START of register role function initialization
FUNCTION register_role (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    ROLE_NAME IN ROLE.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS role_id NUMBER;

user_role_name NVARCHAR2 (128);

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name != 'admin' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;
    
    variable_array := increamental_array (ROLE_NAME);
    variable_name_array := increamental_array ('ROLE_NAME');
    regex_array := increamental_array ('^[[:alpha:][:space:]]{1,255}$');
    error_info_array := increamental_array ('role name only accept alphabet');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        INSERT INTO
            role
        VALUES
            (role_sequence.NEXTVAL, ROLE_NAME) RETURNING ROLE.ROLE_ID INTO register_role.role_id;
        
        success_object ('registered_role_id') := register_role.role_id;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in register role';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in register role';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in register role';
            ROLLBACK;
            RETURN error_object;
END;
--- END of register role  function initialization

--- START of update role function initialization
FUNCTION update_role (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    ROLE_ID IN ROLE.ROLE_ID % TYPE,
    ROLE_NAME IN ROLE.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

user_role_name NVARCHAR2 (128);
returned_role_id  NUMBER;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = update_role.USER_ID);

    IF user_role_name != 'admin' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;
    variable_array := increamental_array (update_role.ROLE_NAME);
    variable_name_array := increamental_array ('ROLE_NAME');
    regex_array := increamental_array ('^[[:alpha:][:space:]]{1,255}$');
    error_info_array := increamental_array ('role name only accept alphabet');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        UPDATE
            role
        SET
            role.NAME = update_role.ROLE_NAME
        WHERE
            role.ROLE_ID = update_role.ROLE_ID
        RETURNING role.role_id into returned_role_id;
        IF returned_role_id is null THEN
            error_object('ERROR') := 'role not found!';
            RAISE validation_error;
        END IF;        
        success_object ('updated_role_id') := returned_role_id;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in update role';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in update role';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + 'in update role';
            ROLLBACK;
            RETURN error_object;
END;
--- END of update role  function initialization

--- START of delete role function initialization
FUNCTION delete_role (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    ROLE_ID IN ROLE.ROLE_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

user_role_name NVARCHAR2 (128);

returned_role_id number;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name != 'admin' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;

    DELETE FROM
        role
    WHERE
        role.ROLE_ID = delete_role.ROLE_ID
    RETURNING role.ROLE_ID INTO returned_role_id;
    
    IF delete_role.returned_role_id is null  THEN 
        error_object ('ERROR') := 'record not found in delete role!';
        RAISE validation_error;
    END IF;

    COMMIT;
    success_object ('deleted_role_id') := delete_role.ROLE_ID;
    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in delete role';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in delete role';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in delete role';
            ROLLBACK;
            RETURN error_object;
END;
--- END of delete role  function initialization

--- START of get roles function initialization
PROCEDURE get_roles (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
)   AS 

user_role_name NVARCHAR2 (128);

BEGIN
    
    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);
    
    IF user_role_name = 'admin' THEN 
        OPEN return_cursor FOR SELECT * FROM role;
    ELSIF user_role_name = 'secretary' THEN 
        OPEN return_cursor FOR SELECT * FROM role where role.NAME not in ('admin');
    END IF;  
END;
--- END of get roles  function initialization
END role_package;

-- END of PACKAGE BODY
--------------------- END of  role TABLE or PACKAGE--------------------------


--------------------- START of  shift TABLE or PACKAGE --------------------------
--  START of PACKAGE Specification
CREATE OR REPLACE PACKAGE shift_package AS 
--- START of register shift function declaretion
FUNCTION register_shift (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    SHIFT_NAME IN SHIFT.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--- START of register shift function declaretion

--  START of update shift function declaretion
FUNCTION update_shift (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    SHIFT_ID IN SHIFT.SHIFT_ID % TYPE,
    SHIFT_NAME IN SHIFT.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of update shift function declaretion

--  START of delete shift function declaretion
FUNCTION delete_shift (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    SHIFT_ID IN SHIFT.SHIFT_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of delete shift function declaretion

--  START of get shifts function declaretion
PROCEDURE get_shifts (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
);
--  END of get shifts function declaretion

END shift_package;
--  END of PACKAGE Specification

--  START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY shift_package AS 
--- START of register shift function initialization
FUNCTION register_shift (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    SHIFT_NAME IN SHIFT.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

shift_id NUMBER;

user_role_name NVARCHAR2 (128);

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name != 'admin' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;

    variable_array := increamental_array (SHIFT_NAME);
    variable_name_array := increamental_array ('SHIFT_NAME');
    regex_array := increamental_array ('^[[:alpha:][:space:]]{1,255}$');
    error_info_array := increamental_array ('shift name should be alphabet');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        INSERT INTO
            shift
        VALUES
            (
                shift_sequence.NEXTVAL,
                register_shift.SHIFT_NAME
            ) RETURNING SHIFT.SHIFT_ID INTO register_shift.shift_id;
        success_object ('shift_id') := register_shift.shift_id;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in register shift';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in register shift';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in register shift';
            ROLLBACK;
            RETURN error_object;
END;
--- END of register shift function initialization

--- START of update shift function initialization
FUNCTION update_shift (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    SHIFT_ID IN SHIFT.SHIFT_ID % TYPE,
    SHIFT_NAME IN SHIFT.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS

user_role_name NVARCHAR2 (128);
returned_shift_id number;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF update_shift.user_role_name != 'admin' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;

    variable_array := increamental_array (update_shift.SHIFT_NAME);
    variable_name_array := increamental_array ('SHIFT_NAME');
    regex_array := increamental_array ('^[[:alpha:][:space:]]{1,255}$');
    error_info_array := increamental_array ('shift should be alphabet');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        UPDATE
            shift
        SET
            shift.NAME = update_shift.SHIFT_NAME
        WHERE
            shift.SHIFT_ID = update_shift.SHIFT_ID
        RETURNING shift.shift_id into returned_shift_id;
        IF returned_shift_id is null THEN
            error_object('ERROR') := 'shift not found!';
            raise validation_error;
        END IF;
        success_object ('updated_shift_id') := update_shift.SHIFT_ID;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'To many records returned in update shift';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in update shift';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + 'in update shift';
            ROLLBACK;
            RETURN error_object;
END;
--- END of update shift function initialization

--- START of delete shift function initialization
FUNCTION delete_shift (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    SHIFT_ID IN SHIFT.SHIFT_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

user_role_name NVARCHAR2 (128);

returned_shift_id number;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF delete_shift.user_role_name != 'admin' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;
    
    DELETE FROM
        shift
    WHERE
        shift.SHIFT_ID = delete_shift.SHIFT_ID
    RETURNING shift.SHIFT_ID INTO delete_shift.returned_shift_id;
    
    IF delete_shift.returned_shift_id is null  THEN 
        error_object ('ERROR') := 'record not found!';
        RAISE validation_error;
    END IF;

    success_object ('deleted_shift_id') := delete_shift.SHIFT_ID;
    COMMIT;
    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'To many records returned in delete shift';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in delete shift';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in delete shift';
            ROLLBACK;
            RETURN error_object;
END;
--- END of delete shift function initialization

--- START of get shifts function initialization
PROCEDURE get_shifts (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
)   AS 

user_role_name varchar2(128);

BEGIN
    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);
    
    IF user_role_name = 'admin' OR user_role_name = 'secretary' OR user_role_name = 'manager' THEN 
        OPEN return_cursor FOR SELECT * FROM shift;
    END IF;
END;
--- END of get shifts  function initialization

end shift_package;
--------------------- END of  shift TABLE or PACKAGE--------------------------

--------------------- START of job TABLE or PACKAGE --------------------------
--  START of PACKAGE Specification
CREATE OR REPLACE PACKAGE job_package AS 
--- START of register job function declaretion
FUNCTION register_job (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    JOB_NAME IN JOB.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--- END of register job function declaretion

--  START of update job function declaretion
FUNCTION update_job (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    JOB_ID IN JOB.JOB_ID % TYPE,
    JOB_NAME IN JOB.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of update job function declaretion

--  START of delete job function declaretion
FUNCTION delete_job (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    JOB_ID IN JOB.JOB_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of delete job function declaretion

--  START of get jobs function declaretion
PROCEDURE get_jobs (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
);
--  END of get jobs function declaretion

END job_package;
--  END of PACKAGE Specification

--  START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY job_package AS 
--- START of register job function initialization
FUNCTION register_job (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    JOB_NAME IN JOB.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

job_id NUMBER;
user_role_name NVARCHAR2 (128);

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name != 'admin' AND user_role_name != 'secretary' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;

    variable_array := increamental_array (JOB_NAME);
    variable_name_array := increamental_array ('JOB_NAME');
    regex_array := increamental_array ('^[[:alpha:][:space:]]{1,255}$');
    error_info_array := increamental_array ('job name should be alphabet!');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        INSERT INTO
            job
        VALUES
            (job_sequence.NEXTVAL, register_job.JOB_NAME) 
        RETURNING JOB.JOB_ID INTO register_job.job_id;

        success_object ('registered_job_id') := register_job.job_id;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in register job';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in register job';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in register job';
            ROLLBACK;
            RETURN error_object;
END;

--- END of register job function initialization

--- START of update job function initialization
FUNCTION update_job (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    JOB_ID IN JOB.JOB_ID % TYPE,
    JOB_NAME IN JOB.NAME % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

user_role_name NVARCHAR2 (128);
returned_job_id number;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name != 'admin' AND user_role_name != 'secretary' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;

    variable_array := increamental_array (update_job.JOB_NAME);
    variable_name_array := increamental_array ('JOB_NAME');
    regex_array := increamental_array ('^[[:alpha:][:space:]]{1,255}$');
    error_info_array := increamental_array ('job name should be alphabet!');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        UPDATE
            job
        SET
            job.NAME = update_job.JOB_NAME
        WHERE
            job.JOB_ID = update_job.JOB_ID
        RETURN job.JOB_ID into returned_job_id;
        
        IF returned_job_id is null THEN
            error_object('ERROR') := 'No record found!';
            raise validation_error;
        END IF;

        success_object ('updated_job_id') := update_job.JOB_ID;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK;
            ROLLBACK;
            RETURN error_object;
END;
--- END of update job function initialization

--- START of delete job function initialization
FUNCTION delete_job (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    JOB_ID IN JOB.JOB_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS

user_role_name VARCHAR2(256);

return_job_id number;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name != 'admin' AND user_role_name != 'secretary' THEN 
        error_object ('ERROR') := 'you do not have the permission!';
        RAISE validation_error;
    END IF;

    DELETE FROM
        job
    WHERE
        job.JOB_ID = delete_job.JOB_ID
    RETURNING job.JOB_ID INTO delete_job.return_job_id;
    
    IF delete_job.return_job_id is null  THEN 
        error_object ('ERROR') := 'record not found!';
        RAISE validation_error;
    END IF;
    
    success_object ('deleted_job_id') := delete_job.return_job_id;
    COMMIT;
    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in delete job';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in delete job';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + 'in delete job';
            ROLLBACK;
            RETURN error_object;
END;

--- END of delete job function initialization

--- START of get jobs function initialization
PROCEDURE get_jobs (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
)   AS 

user_role_name varchar2(128);

BEGIN
    
    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);

    IF user_role_name = 'admin' OR user_role_name = 'secretary' OR user_role_name = 'manager' THEN 
        OPEN return_cursor FOR SELECT * FROM job;
    END IF;

END;
--- END of get jobs  function initialization


END job_package;
-- END of PACKAGE BODY
--------------------- END of  job TABLE or PACKAGE------------------------------


--------------------- START of Auth TABLE or PACKAGE --------------------------
--  START of PACKAGE Specification
CREATE OR REPLACE PACKAGE auth_package AS 
--- START of register auth function declaretion
FUNCTION register_auth (
    EMPLOYEE_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
    password IN AUTH.PASSWORD %TYPE,
    TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--- END of register auth function declaretion

--  START of update auth function declaretion
FUNCTION update_auth (
    AUTH_ID IN AUTH.AUTH_ID % TYPE,
    PASSWORD IN AUTH.PASSWORD % TYPE,
    TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of update auth function declaretion

END auth_package;
--  END of PACKAGE Specification

--  START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY auth_package AS 
--- START of register auth function initialization
FUNCTION register_auth (
    EMPLOYEE_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
    PASSWORD IN AUTH.PASSWORD %TYPE,
    TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

auth_id NUMBER;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN
    variable_array := increamental_array (register_auth.TWO_FACTOR);
    variable_name_array := increamental_array ('TWO_FACTOR');
    regex_array := increamental_array ('(^[01]$)|(null)|(NULL)');
    error_info_array := increamental_array ('two factor can only accept 0 or 1');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        INSERT INTO
            auth(auth.AUTH_ID, auth.PASSWORD, auth.TWO_FACTOR, auth.EMPLOYEE_ID)
        VALUES
            (auth_sequence.NEXTVAL, register_auth.PASSWORD, register_auth.TWO_FACTOR, register_auth.EMPLOYEE_ID) 
        RETURNING AUTH.AUTH_ID INTO register_auth.auth_id;

        success_object ('registered_auth_id') := register_auth.auth_id;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in auth register';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in auth register';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in auth register';
            ROLLBACK;
            RETURN error_object;
END;

--- END of register auth function initialization

--- START of update auth function initialization
FUNCTION update_auth (
    AUTH_ID IN AUTH.AUTH_ID % TYPE,
    PASSWORD IN AUTH.PASSWORD % TYPE,
    TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

returned_auth_id number;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;

BEGIN

    variable_array := increamental_array (update_auth.TWO_FACTOR);
    variable_name_array := increamental_array ('TWO_FACTOR');
    regex_array := increamental_array ('(^[01]$)|(null)|(NULL)');
    error_info_array := increamental_array ('TWO FACTOR must be 0 or 1');

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        variable_array,
        variable_name_array,
        regex_array,
        error_info_array
    );

    IF error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE
        UPDATE
            auth
        SET
            auth.PASSWORD = update_auth.PASSWORD,
            auth.TWO_FACTOR = update_auth.TWO_FACTOR,
            auth.UPDATEDAT = sysdate
        WHERE
            AUTH.AUTH_ID = update_auth.AUTH_ID
        RETURNING AUTH.AUTH_ID into returned_auth_id;
        IF returned_auth_id IS NULL THEN 
            error_object('ERROR') := 'auth id is not exist!';
            raise validation_error;
        END IF;
        success_object ('updated_auth_id') :=  update_auth.AUTH_ID;
        COMMIT;
    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            error_object ('ERROR') := 'Too many records returned in update auth';
            RETURN error_object;
        WHEN NO_DATA_FOUND THEN 
            error_object ('ERROR') := 'No record found in update auth';
            ROLLBACK;
            RETURN error_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN error_object;
        WHEN OTHERS THEN 
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in update auth';
            ROLLBACK;
            RETURN error_object;
END;
--- END of update auth function initialization

END auth_package;
-- END of PACKAGE BODY
--------------------- END of  auth TABLE or PACKAGE------------------------------



--------------------- START of employee TABLE or PACKAGE -----------------------
--  START of PACKAGE Specification
CREATE OR REPLACE PACKAGE employee_package AS 
--  START of employee data validation function declaretion
FUNCTION employee_data_validation (
    FIRST_NAME IN EMPLOYEE.FIRST_NAME % TYPE,
    LAST_NAME IN EMPLOYEE.LAST_NAME % TYPE,
    SSN IN EMPLOYEE.SSN % TYPE,
    PHOTO IN EMPLOYEE.PHOTO % TYPE,
    HIRE_DATE IN EMPLOYEE.HIRE_DATE % TYPE,
    HAS_PASSWORD IN EMPLOYEE.HAS_PASSWORD%TYPE,
    ROLE_ID IN EMPLOYEE.ROLE_ID % TYPE,
    SHIFT_ID IN EMPLOYEE.SHIFT_ID % TYPE,
    JOB_ID IN EMPLOYEE.JOB_ID % TYPE,
    SALARY IN EMPLOYEE.SALARY % TYPE,
    CONTRACT_FILE IN EMPLOYEE.CONTRACT_FILE % TYPE,
    CV_LINK IN EMPLOYEE.CV_LINK % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of employee data validation function declaretion

--  START of register employee function declaretion
FUNCTION register_employee (
    R_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
    R_COUNTRY IN ADDRESS.COUNTRY % TYPE,
    R_CITY IN ADDRESS.CITY % TYPE,
    R_DISTRICT IN ADDRESS.DISTRICT % TYPE,
    R_HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    R_MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE,
    R_PHONE IN CONTACT.PHONE % TYPE,
    R_EMAIL IN CONTACT.EMAIL % TYPE,
    R_FIRST_NAME IN EMPLOYEE.FIRST_NAME % TYPE,
    R_LAST_NAME IN EMPLOYEE.LAST_NAME % TYPE,
    R_SSN IN EMPLOYEE.SSN % TYPE,
    R_PHOTO IN EMPLOYEE.PHOTO % TYPE,
    R_HIRE_DATE IN EMPLOYEE.HIRE_DATE % TYPE,
    R_HAS_PASSWORD IN EMPLOYEE.HAS_PASSWORD%TYPE,
    R_PASSWORD IN AUTH.PASSWORD%TYPE,
    R_TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE,
    R_ROLE_ID IN EMPLOYEE.ROLE_ID%TYPE,
    R_SHIFT_ID IN EMPLOYEE.SHIFT_ID % TYPE,
    R_JOB_ID IN EMPLOYEE.JOB_ID % TYPE,
    R_SALARY IN EMPLOYEE.SALARY % TYPE,
    R_CONTRACT_FILE IN EMPLOYEE.CONTRACT_FILE % TYPE,
    R_CV_LINK IN EMPLOYEE.CV_LINK % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of register employee function declaretion

--  START of update employee function declaretion
FUNCTION update_employee (
    U_USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    U_EMPLOYEE_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    U_COUNTRY IN ADDRESS.COUNTRY % TYPE,
    U_CITY IN ADDRESS.CITY % TYPE,
    U_DISTRICT IN ADDRESS.DISTRICT % TYPE,
    U_HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    U_MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE,
    U_PHONE IN CONTACT.PHONE % TYPE,
    U_EMAIL IN CONTACT.EMAIL % TYPE,
    U_FIRST_NAME IN EMPLOYEE.FIRST_NAME % TYPE,
    U_LAST_NAME IN EMPLOYEE.LAST_NAME % TYPE,
    U_SSN IN EMPLOYEE.SSN % TYPE,
    U_PHOTO IN EMPLOYEE.PHOTO % TYPE,
    U_HIRE_DATE IN EMPLOYEE.HIRE_DATE % TYPE,
    U_HAS_PASSWORD IN EMPLOYEE.HAS_PASSWORD%TYPE,
    U_PASSWORD IN AUTH.PASSWORD%TYPE,
    U_TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE,
    U_ROLE_ID IN EMPLOYEE.ROLE_ID % TYPE,
    U_SHIFT_ID IN EMPLOYEE.SHIFT_ID % TYPE,
    U_JOB_ID IN EMPLOYEE.JOB_ID % TYPE,
    U_SALARY IN EMPLOYEE.SALARY % TYPE,
    U_CONTRACT_FILE IN EMPLOYEE.CONTRACT_FILE % TYPE,
    U_CV_LINK IN EMPLOYEE.CV_LINK % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of update employee function declaretion

--  START of delete employee function declaretion
FUNCTION delete_employee (
    D_USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    D_EMPLOYEE_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object;
--  END of delete employee function declaretion

--  START of get employees function declaretion
PROCEDURE get_employees (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
);
--  END of get employees function declaretion

--- START of get employee function declaretion
PROCEDURE get_employee (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    USER_EMAIL IN CONTACT.EMAIL % TYPE,
    return_cursor out  SYS_REFCURSOR 
);
--- END of get employee function declaretion


END employee_package;
--  END of PACKAGE Specification

--  START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY employee_package AS 
--- START of employee data validating function initialization
FUNCTION employee_data_validation (
    FIRST_NAME IN EMPLOYEE.FIRST_NAME % TYPE,
    LAST_NAME IN EMPLOYEE.LAST_NAME % TYPE,
    SSN IN EMPLOYEE.SSN % TYPE,
    PHOTO IN EMPLOYEE.PHOTO % TYPE,
    HIRE_DATE IN EMPLOYEE.HIRE_DATE % TYPE,
    HAS_PASSWORD IN EMPLOYEE.HAS_PASSWORD%TYPE,
    ROLE_ID IN EMPLOYEE.ROLE_ID % TYPE,
    SHIFT_ID IN EMPLOYEE.SHIFT_ID % TYPE,
    JOB_ID IN EMPLOYEE.JOB_ID % TYPE,
    SALARY IN EMPLOYEE.SALARY % TYPE,
    CONTRACT_FILE IN EMPLOYEE.CONTRACT_FILE % TYPE,
    CV_LINK IN EMPLOYEE.CV_LINK % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

is_ssn number;

variable_array increamental_array;
variable_name_array increamental_array;
regex_array increamental_array;
error_info_array increamental_array;

success_object VALIDATION_PACKAGE.increamental_object;
error_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN 
    variable_array := increamental_array (
        employee_data_validation.FIRST_NAME,
        employee_data_validation.LAST_NAME,
        employee_data_validation.SSN,
        employee_data_validation.PHOTO,
        employee_data_validation.HIRE_DATE,
        employee_data_validation.HAS_PASSWORD,
        employee_data_validation.ROLE_ID,
        employee_data_validation.SHIFT_ID,
        employee_data_validation.JOB_ID,
        employee_data_validation.SALARY,
        employee_data_validation.CONTRACT_FILE,
        employee_data_validation.CV_LINK
    );

    variable_name_array := increamental_array (
        'FIRST_NAME',
        'LAST_NAME',
        'SSN',
        'PHOTO',
        'HIRE_DATE',
        'HAS_PASSWORD',
        'ROLE_ID',
        'SHIFT_ID',
        'JOB_ID',
        'SALARY',
        'HAS_PASSWORD',
        'CONTRACT_FILE',
        'CV_LINK'
    );
    regex_array := increamental_array (
        '^[[:alpha:][:space:]]{1,255}$',
        '^[[:alpha:][:space:]]{1,255}$',
        '^[[:digit:]]{0,20}$',
        '(^[[:alnum:][:punct:]]{1,1023}$)|(null)|(NULL)',
        '(^([[:digit:]]{1,2})\/([[:digit:]]{1,2})\/([[:digit:]]{1,4})$)|(null)|(NULL)',
        '(^[01]$)|(null)|(NULL)',
        '^[[:digit:]]{0,}$',
        '^[[:digit:]]{0,}$',
        '^[[:digit:]]{0,}$',
        '(^([[:digit:]]{0,})(\.[[:digit:]]{0,})?$)|(null)|(NULL)',
        '^[[:alnum:][:punct:]]{1,1023}$',
        '(^[[:alnum:][:punct:]]{1,1023}$)|(null)|(NULL)'
    );

    error_info_array := increamental_array (
        'FIRST NAME should be of type string',
        'LAST NAME should be of type string',
        'SSN should of type number and should be unique',
        'PHOTO should be of type image and it should be a valid link',
        'HIRE DATE should be of type date in the following format day/mount/year',
        'HAS_PASSWORD can only accept 0 or 1',
        'ROLE ID should be of type number and unique',
        'SHIFT ID should be of type number and unique',
        'JOB ID should be of type number and unique',
        'SALARY should be of type real number',
        'CONTRACT FILE should be of type link',
        'CV LINK should be of type link'
    );

    error_object := VALIDATION_PACKAGE.DATA_VALIDATION (
        employee_data_validation.variable_array,
        employee_data_validation.variable_name_array,
        employee_data_validation.regex_array,
        employee_data_validation.error_info_array
    );

    IF employee_data_validation.error_object.COUNT > 0 THEN 
        RAISE validation_error;
    ELSE 
        select (select ssn from employee where employee.ssn = employee_data_validation.SSN) into is_ssn from dual;
        IF is_ssn is not null THEN
            error_object('ERROR') := 'ssn must be unique';
            RAISE validation_error;
        END IF; 
        success_object ('employee_data_validated') := 'succeed';
    END IF;
    RETURN success_object;

    EXCEPTION
        WHEN validation_error THEN 
        RETURN error_object;
        WHEN OTHERS THEN
            error_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK + ' in employee validation function';
            RETURN error_object;
END;
--- END of employee data validating function initialization

--  START of register employee function initialization
FUNCTION register_employee (
    R_USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    R_COUNTRY IN ADDRESS.COUNTRY % TYPE,
    R_CITY IN ADDRESS.CITY % TYPE,
    R_DISTRICT IN ADDRESS.DISTRICT % TYPE,
    R_HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    R_MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE,
    R_PHONE IN CONTACT.PHONE % TYPE,
    R_EMAIL IN CONTACT.EMAIL % TYPE,
    R_FIRST_NAME IN EMPLOYEE.FIRST_NAME % TYPE,
    R_LAST_NAME IN EMPLOYEE.LAST_NAME % TYPE,
    R_SSN IN EMPLOYEE.SSN % TYPE,
    R_PHOTO IN EMPLOYEE.PHOTO % TYPE,
    R_HIRE_DATE IN EMPLOYEE.HIRE_DATE % TYPE,
    R_HAS_PASSWORD IN EMPLOYEE.HAS_PASSWORD%TYPE,
    R_PASSWORD IN AUTH.PASSWORD%TYPE,
    R_TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE,
    R_ROLE_ID IN EMPLOYEE.ROLE_ID % TYPE,
    R_SHIFT_ID IN EMPLOYEE.SHIFT_ID % TYPE,
    R_JOB_ID IN EMPLOYEE.JOB_ID % TYPE,
    R_SALARY IN EMPLOYEE.SALARY % TYPE,
    R_CONTRACT_FILE IN EMPLOYEE.CONTRACT_FILE % TYPE,
    R_CV_LINK IN EMPLOYEE.CV_LINK % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

address_id NUMBER;
contact_id NUMBER;
employee_id NUMBER;
auth_id number;

user_role_name NVARCHAR2 (256);
employee_role_name NVARCHAR2 (256);

success_object VALIDATION_PACKAGE.increamental_object;
response_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN
    
    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = (select employee.ROLE_ID from employee where employee.employee_id = register_employee.R_USER_ID);        
    SELECT ROLE.NAME INTO employee_role_name FROM role WHERE role.role_id = register_employee.R_ROLE_ID;

    IF register_employee.employee_role_name = 'admin' THEN 
        response_object ('ERROR') := 'More then on employee can not have the Admin role!';
        RAISE validation_error;
    ELSIF (
        register_employee.user_role_name = 'secretary'
        AND register_employee.employee_role_name = 'secretary'
    ) THEN 
        response_object ('ERROR') := 'secretary role employee can not add another secretary role employee!';
        RAISE validation_error;
    END IF;
    
    IF register_employee.user_role_name = 'admin' OR register_employee.user_role_name = 'secretary' THEN 
        response_object := CONTACT_PACKAGE.REGISTER_CONTACT (
            register_employee.R_COUNTRY,
            register_employee.R_CITY,
            register_employee.R_DISTRICT,
            register_employee.R_HOME_NUMBER,
            register_employee.R_MAP_LOCATION,
            register_employee.R_PHONE,
            register_employee.R_EMAIL
        );

        IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_contact_id' THEN 
            RAISE validation_error;
        ELSE 
            register_employee.contact_id := response_object (response_object.FIRST);
        END IF;

        response_object := EMPLOYEE_PACKAGE.EMPLOYEE_DATA_VALIDATION (
            register_employee.R_FIRST_NAME,
            register_employee.R_LAST_NAME,
            register_employee.R_SSN,
            register_employee.R_PHOTO,
            register_employee.R_HIRE_DATE,
            register_employee.R_HAS_PASSWORD,
            register_employee.R_ROLE_ID,
            register_employee.R_SHIFT_ID,
            register_employee.R_JOB_ID,
            register_employee.R_SALARY,
            register_employee.R_CONTRACT_FILE,
            register_employee.R_CV_LINK
        );

        IF response_object.COUNT > 0 AND response_object (response_object.FIRST) != 'succeed' THEN 
            RAISE validation_error;
        ELSE
            
            INSERT INTO
                employee
            VALUES
                (
                    employee_sequence.NEXTVAL,
                    register_employee.R_FIRST_NAME,
                    register_employee.R_LAST_NAME,
                    register_employee.R_SSN,
                    register_employee.R_PHOTO,
                    register_employee.R_HIRE_DATE,
                    register_employee.contact_id,
                    register_employee.R_ROLE_ID,
                    register_employee.R_SHIFT_ID,
                    register_employee.R_JOB_ID,
                    register_employee.R_SALARY,
                    register_employee.R_HAS_PASSWORD,
                    register_employee.R_CONTRACT_FILE,
                    register_employee.R_CV_LINK
                ) RETURNING EMPLOYEE.EMPLOYEE_ID INTO register_employee.employee_id;
            
            IF register_employee.R_HAS_PASSWORD = 1 THEN
                response_object := auth_package.REGISTER_AUTH(register_employee.employee_id, register_employee.R_PASSWORD, register_employee.R_TWO_FACTOR);
                IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_auth_id' THEN 
                    RAISE validation_error;
                END IF;
            END IF;
            success_object ('registered_employee_id') := register_employee.employee_id;
            COMMIT;
        END IF;

    ELSE response_object ('ERROR') := 'You do not have permission';
        RAISE validation_error;
    END IF;
    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            response_object ('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN 
            ROLLBACK;
            response_object ('ERROR') := 'No record found';
            RETURN response_object;
        WHEN DUP_VAL_ON_INDEX THEN
            ROLLBACK;
            response_object ('ERROR') := 'Unique constraint error';
            RETURN response_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN 
            ROLLBACK;
            response_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK;
            RETURN response_object;
END;

--  END of register employee function initialization

--  START of update employee function initialization
FUNCTION update_employee (
    U_USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    U_EMPLOYEE_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    U_COUNTRY IN ADDRESS.COUNTRY % TYPE,
    U_CITY IN ADDRESS.CITY % TYPE,
    U_DISTRICT IN ADDRESS.DISTRICT % TYPE,
    U_HOME_NUMBER IN ADDRESS.HOME_NUMBER % TYPE,
    U_MAP_LOCATION IN ADDRESS.MAP_LOCATION % TYPE,
    U_PHONE IN CONTACT.PHONE % TYPE,
    U_EMAIL IN CONTACT.EMAIL % TYPE,
    U_FIRST_NAME IN EMPLOYEE.FIRST_NAME % TYPE,
    U_LAST_NAME IN EMPLOYEE.LAST_NAME % TYPE,
    U_SSN IN EMPLOYEE.SSN % TYPE,
    U_PHOTO IN EMPLOYEE.PHOTO % TYPE,
    U_HIRE_DATE IN EMPLOYEE.HIRE_DATE % TYPE,
    U_HAS_PASSWORD IN EMPLOYEE.HAS_PASSWORD%TYPE,
    U_PASSWORD IN AUTH.PASSWORD%TYPE,
    U_TWO_FACTOR IN AUTH.TWO_FACTOR%TYPE,
    U_ROLE_ID IN EMPLOYEE.ROLE_ID % TYPE,
    U_SHIFT_ID IN EMPLOYEE.SHIFT_ID % TYPE,
    U_JOB_ID IN EMPLOYEE.JOB_ID % TYPE,
    U_SALARY IN EMPLOYEE.SALARY % TYPE,
    U_CONTRACT_FILE IN EMPLOYEE.CONTRACT_FILE % TYPE,
    U_CV_LINK IN EMPLOYEE.CV_LINK % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS 

contact_id NUMBER;
auth_id number;
employee_has_password number;
return_employee_id number;
return_auth_id number;


employee_role_name NVARCHAR2 (256);
user_role_name NVARCHAR2 (256);

success_object VALIDATION_PACKAGE.increamental_object;
response_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN

    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = update_employee.U_USER_ID);
    SELECT ROLE.NAME INTO employee_role_name FROM role WHERE role.role_id = update_employee.U_ROLE_ID;

    IF (update_employee.employee_role_name = 'admin') THEN
        response_object ('ERROR') := 'Admain role can not be changed!';
        RAISE validation_error;
    ELSIF (update_employee.user_role_name = 'secretary' AND update_employee.employee_role_name = 'secretary') THEN
        response_object ('ERROR') := 'secretary role can not be changed by another secretary role!';
        RAISE validation_error;
    END IF;

    IF update_employee.user_role_name = 'admin' OR update_employee.user_role_name = 'secretary' THEN
        SELECT
            EMPLOYEE.HAS_PASSWORD INTO update_employee.employee_has_password
        FROM
            EMPLOYEE
        WHERE
            EMPLOYEE.EMPLOYEE_ID = update_employee.U_EMPLOYEE_ID;

        IF update_employee.employee_has_password = 1 THEN 
            SELECT
                AUTH.AUTH_ID INTO update_employee.auth_id
            FROM
                AUTH
            WHERE
                AUTH.EMPLOYEE_ID = update_employee.U_EMPLOYEE_ID;            
        END IF;
        
        select (select contact_id from employee where employee_id = 42) into contact_id from dual;
        IF update_employee.contact_id is not null THEN
            response_object := CONTACT_PACKAGE.UPDATE_CONTACT (
                update_employee.contact_id,
                update_employee.U_COUNTRY,
                update_employee.U_CITY,
                update_employee.U_DISTRICT,
                update_employee.U_HOME_NUMBER,
                update_employee.U_MAP_LOCATION,
                update_employee.U_PHONE,
                update_employee.U_EMAIL
            );
            IF response_object.COUNT > 0 AND response_object.FIRST != 'updated_contact_id' THEN 
                RAISE validation_error;
            END IF;        
        ELSE
            response_object := CONTACT_PACKAGE.REGISTER_CONTACT (
            update_employee.U_COUNTRY,
            update_employee.U_CITY,
            update_employee.U_DISTRICT,
            update_employee.U_HOME_NUMBER,
            update_employee.U_MAP_LOCATION,
            update_employee.U_PHONE,
            update_employee.U_EMAIL
            );

            IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_contact_id' THEN 
                RAISE validation_error;
            ELSE 
                update_employee.contact_id := response_object (response_object.FIRST);
            END IF;
        END IF;
        
        response_object := EMPLOYEE_PACKAGE.EMPLOYEE_DATA_VALIDATION (
            update_employee.U_FIRST_NAME,
            update_employee.U_LAST_NAME,
            update_employee.U_SSN,
            update_employee.U_PHOTO,
            update_employee.U_HIRE_DATE,
            update_employee.U_HAS_PASSWORD,
            update_employee.U_ROLE_ID,
            update_employee.U_SHIFT_ID,
            update_employee.U_JOB_ID,
            update_employee.U_SALARY,
            update_employee.U_CONTRACT_FILE,
            update_employee.U_CV_LINK
        );
          
        
        IF response_object(response_object.FIRST) = 'succeed' THEN
            IF update_employee.employee_has_password = 1 AND update_employee.U_HAS_PASSWORD = 1 THEN 
                response_object := AUTH_PACKAGE.UPDATE_AUTH(update_employee.auth_id, update_employee.U_PASSWORD, update_employee.U_TWO_FACTOR);
                IF response_object.COUNT > 0 AND response_object.FIRST != 'updated_auth_id' THEN 
                    RAISE validation_error;
                END IF;
            ELSIF update_employee.employee_has_password = 0 AND update_employee.U_HAS_PASSWORD = 1 THEN
                response_object := auth_package.REGISTER_AUTH(update_employee.U_EMPLOYEE_ID, update_employee.U_PASSWORD, update_employee.U_TWO_FACTOR);
                IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_auth_id' THEN 
                    RAISE validation_error;
                END IF;
            ELSIF update_employee.employee_has_password = 1 AND update_employee.U_HAS_PASSWORD = 0 THEN
                delete from auth where auth.EMPLOYEE_ID = update_employee.U_EMPLOYEE_ID RETURN auth.AUTH_ID INTO update_employee.return_auth_id;
                IF update_employee.return_auth_id is null  THEN 
                    response_object('ERROR ') := 'no auth exists for that employee'; 
                    RAISE validation_error;
                END IF;
            END IF;
            
            UPDATE
                employee
            SET
                EMPLOYEE.FIRST_NAME = update_employee.U_FIRST_NAME,
                EMPLOYEE.LAST_NAME = update_employee.U_LAST_NAME,
                EMPLOYEE.SSN = update_employee.U_SSN,
                EMPLOYEE.PHOTO = update_employee.U_PHOTO,
                EMPLOYEE.HIRE_DATE = update_employee.U_HIRE_DATE,
                EMPLOYEE.HAS_PASSWORD = update_employee.U_HAS_PASSWORD,
                EMPLOYEE.CONTACT_ID = update_employee.contact_id,
                EMPLOYEE.ROLE_ID = update_employee.U_ROLE_ID,
                EMPLOYEE.SHIFT_ID = update_employee.U_SHIFT_ID,
                EMPLOYEE.JOB_ID = update_employee.U_JOB_ID,
                EMPLOYEE.SALARY = update_employee.U_SALARY,
                EMPLOYEE.CONTRACT_FILE = update_employee.U_CONTRACT_FILE,
                EMPLOYEE.CV_LINK = update_employee.U_CV_LINK
            WHERE
                employee.employee_id = update_employee.U_EMPLOYEE_ID            
            RETURNING employee.employee_id INTO update_employee.return_employee_id;
    
            IF update_employee.return_employee_id is null  THEN 
                response_object ('ERROR') := 'record can not updated due to wrong input!';
                RAISE validation_error;
            END IF;

            success_object ('employee_updated') := 'succeed';
            COMMIT;
        ELSE 
            RAISE validation_error;
        END IF;
    ELSE 
        response_object ('ERROR') := 'You do not have permission';
        RAISE validation_error;
    END IF;

RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            response_object ('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN 
            response_object ('ERROR') := 'No record found in update employee';
            ROLLBACK;
            RETURN response_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN 
            response_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK;
            ROLLBACK;
            RETURN response_object;
END;
--  END of update employee function initialization

--  START of delete employee function initialization
FUNCTION delete_employee (
    D_USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    D_EMPLOYEE_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE
) RETURN VALIDATION_PACKAGE.increamental_object IS address_id NUMBER;

contact_id NUMBER;

employee_role_name NVARCHAR2 (256);
user_role_name NVARCHAR2 (256);

success_object VALIDATION_PACKAGE.increamental_object;
response_object VALIDATION_PACKAGE.increamental_object;

validation_error EXCEPTION;
BEGIN
    select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = D_USER_ID);        
    select ROLE.NAME into employee_role_name from role where ROLE.ROLE_ID = 
        (select employee.ROLE_ID from employee where employee.employee_id = D_EMPLOYEE_ID);

    IF (delete_employee.employee_role_name = 'admin') THEN 
        response_object ('ERROR') := 'Admain role can not be changed!';
        RAISE validation_error;
    ELSIF (delete_employee.user_role_name = 'secretary' AND delete_employee.employee_role_name = 'secretary') THEN 
        response_object ('ERROR') := 'secretary role can not be changed by another secretary role!';
        RAISE validation_error;
    END IF;

    IF delete_employee.user_role_name = 'admin' OR delete_employee.user_role_name = 'secretary' THEN
        SELECT
            EMPLOYEE.CONTACT_ID INTO delete_employee.contact_id
        FROM
            EMPLOYEE
        WHERE
            EMPLOYEE.EMPLOYEE_ID = delete_employee.D_EMPLOYEE_ID;

        SELECT
            CONTACT.ADDRESS_ID INTO delete_employee.address_id
        FROM
            CONTACT
        WHERE
            CONTACT.CONTACT_ID = delete_employee.contact_id;

        response_object := ADDRESS_PACKAGE.DELETE_ADDRESS (delete_employee.address_id);

        IF response_object.COUNT > 0 AND response_object.FIRST != 'deleted_address_id' THEN 
            RAISE validation_error;
        END IF;

        response_object := CONTACT_PACKAGE.DELETE_CONTACT (delete_employee.contact_id);

        IF response_object.COUNT > 0 AND response_object.FIRST != 'deleted_contact_id' THEN 
            RAISE validation_error;
        END IF;

        DELETE FROM
            employee
        WHERE
            employee.employee_id = delete_employee.D_EMPLOYEE_ID;

        success_object ('employee_deleted') := 'succeed';
        COMMIT;
    ELSE 
        response_object ('ERROR') := 'You do not have permission';
        RAISE validation_error;
    END IF;
    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN 
            ROLLBACK;
            response_object ('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN 
            response_object ('ERROR') := 'No record found';
            ROLLBACK;
            RETURN response_object;
        WHEN validation_error THEN 
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN 
            response_object ('ERROR') := DBMS_UTILITY.FORMAT_ERROR_STACK;
            ROLLBACK;
            RETURN response_object;
END;
--  END of delete employee function initialization


--- START of get employees function initialization
PROCEDURE get_employees (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    return_cursor out  SYS_REFCURSOR 
)   AS 

user_role_name varchar2(256);

BEGIN
        select ROLE.NAME into user_role_name from role where ROLE.ROLE_ID = 
            (select employee.ROLE_ID from employee where employee.employee_id = USER_ID);
        IF user_role_name = 'admin' THEN
            OPEN return_cursor FOR 
            SELECT 
                 contact.*, address.*, ROLE.NAME as role_name, SHIFT.NAME as shift_name, JOB.NAME as job_name, employee.*, auth.*
            FROM employee
                FULL OUTER JOIN contact on employee.contact_id = CONTACT.CONTACT_ID
                FULL OUTER JOIN address on CONTACT.ADDRESS_ID = ADDRESS.ADDRESS_ID
                FULL OUTER JOIN role on EMPLOYEE.ROLE_ID = ROLE.ROLE_ID
                FULL OUTER JOIN shift on EMPLOYEE.SHIFT_ID = SHIFT.SHIFT_ID
                FULL OUTER JOIN job on EMPLOYEE.JOB_ID = JOB.JOB_ID
                FULL OUTER JOIN auth on auth.EMPLOYEE_ID = employee.EMPLOYEE_ID
            WHERE EMPLOYEE.EMPLOYEE_ID IS NOT NULL;
        ELSIF user_role_name = 'secretary' THEN
            OPEN return_cursor FOR 
            SELECT 
                 contact.*, address.*, ROLE.NAME as role_name, SHIFT.NAME as shift_name, JOB.NAME as job_name, employee.*
            FROM employee
                FULL OUTER JOIN contact on employee.contact_id = CONTACT.CONTACT_ID
                FULL OUTER JOIN address on CONTACT.ADDRESS_ID = ADDRESS.ADDRESS_ID
                FULL OUTER JOIN role on EMPLOYEE.ROLE_ID = ROLE.ROLE_ID
                FULL OUTER JOIN shift on EMPLOYEE.SHIFT_ID = SHIFT.SHIFT_ID
                FULL OUTER JOIN job on EMPLOYEE.JOB_ID = JOB.JOB_ID
            WHERE role.NAME not in ('admin') AND EMPLOYEE.EMPLOYEE_ID IS NOT NULL;
        ELSIF user_role_name = 'manager' THEN
            OPEN return_cursor FOR 
            SELECT 
                 contact.PHONE, contact.EMAIL, SHIFT.NAME as shift_name, JOB.NAME as job_name, employee.EMPLOYEE_ID, employee.FIRST_NAME, employee.LAST_NAME, employee.PHOTO, employee.CV_LINK
            FROM employee
                FULL OUTER JOIN contact on employee.contact_id = CONTACT.CONTACT_ID
                FULL OUTER JOIN shift on EMPLOYEE.SHIFT_ID = SHIFT.SHIFT_ID
                FULL OUTER JOIN job on EMPLOYEE.JOB_ID = JOB.JOB_ID
            WHERE EMPLOYEE.EMPLOYEE_ID IS NOT NULL;
        ELSIF user_role_name = 'supervisor' THEN
            OPEN return_cursor FOR 
            SELECT 
                 contact.PHONE, contact.EMAIL, JOB.NAME as job_name, employee.EMPLOYEE_ID, employee.FIRST_NAME, employee.LAST_NAME, employee.PHOTO
            FROM employee
                FULL OUTER JOIN contact on employee.contact_id = CONTACT.CONTACT_ID
                FULL OUTER JOIN job on EMPLOYEE.JOB_ID = JOB.JOB_ID
            WHERE EMPLOYEE.EMPLOYEE_ID IS NOT NULL;
        END IF;
END;
--- END of get employees  function initialization


--- START of get employee function initialization
PROCEDURE get_employee (
    USER_ID IN EMPLOYEE.EMPLOYEE_ID % TYPE,
    USER_EMAIL IN CONTACT.EMAIL % TYPE,
    return_cursor out  SYS_REFCURSOR
)   AS 

BEGIN
        IF USER_EMAIL IS NOT NULL THEN
            OPEN return_cursor FOR 
            SELECT 
                 contact.*, address.*, ROLE.NAME as role_name, SHIFT.NAME as shift_name, JOB.NAME as job_name, employee.*, auth.*
            FROM employee
                FULL OUTER JOIN contact on employee.contact_id = CONTACT.CONTACT_ID
                FULL OUTER JOIN address on CONTACT.ADDRESS_ID = ADDRESS.ADDRESS_ID
                FULL OUTER JOIN role on EMPLOYEE.ROLE_ID = ROLE.ROLE_ID
                FULL OUTER JOIN shift on EMPLOYEE.SHIFT_ID = SHIFT.SHIFT_ID
                FULL OUTER JOIN job on EMPLOYEE.JOB_ID = JOB.JOB_ID
                FULL OUTER JOIN auth on auth.EMPLOYEE_ID = employee.EMPLOYEE_ID
            WHERE contact.EMAIL = USER_EMAIL AND EMPLOYEE.EMPLOYEE_ID IS NOT NULL;
        ELSIF USER_ID IS NOT NULL THEN
            OPEN return_cursor FOR 
            SELECT 
                 contact.*, address.*, ROLE.NAME as role_name, SHIFT.NAME as shift_name, JOB.NAME as job_name, employee.*, auth.*
            FROM employee
                FULL OUTER JOIN contact on employee.contact_id = CONTACT.CONTACT_ID
                FULL OUTER JOIN address on CONTACT.ADDRESS_ID = ADDRESS.ADDRESS_ID
                FULL OUTER JOIN role on EMPLOYEE.ROLE_ID = ROLE.ROLE_ID
                FULL OUTER JOIN shift on EMPLOYEE.SHIFT_ID = SHIFT.SHIFT_ID
                FULL OUTER JOIN job on EMPLOYEE.JOB_ID = JOB.JOB_ID
                FULL OUTER JOIN auth on auth.EMPLOYEE_ID = employee.EMPLOYEE_ID
            WHERE employee.EMPLOYEE_ID = USER_ID;     
        END IF;
END;
--- END of get employee  function initialization

END employee_package;

-- END of PACKAGE BODY
--------------------- END of employee TABLE or PACKAGE--------------------------
-- please run this function if you get a date error
ALTER SESSION
SET
    NLS_DATE_FORMAT = 'DD/MM/YYYY';


 -- CHECK THE ABOVE FUNCTIONS BY below Code
/* 
 DECLARE
 response_object   VALIDATION_PACKAGE.increamental_object;
 object_item       VARCHAR2 (512);
 BEGIN
 response_object := JOB_PACKAGE.UPDATE_JOB(42, 81, 'enginers');
 
 object_item := response_object.FIRST;
 
 WHILE object_item IS NOT NULL
 LOOP
 DBMS_OUTPUT.PUT_LINE (
 'attribute: '
 || object_item
 || ' ERROR: '
 || response_object (object_item));
 object_item := response_object.NEXT (object_item);
 END LOOP;
 END;
*/
 /*
 desc employee;
 select * from employee;
 insert into job values(shift_sequence.nextval, 'owner');
 
 insert into employee(employee_id, first_name, last_name, ssn, role_id, shift_id, job_id, contract_file)
 values(employee_sequence.nextval, 'abdul qadir', 'agha', 122234,102 , 62, 66, 'https/localhost/file')
 */
 
