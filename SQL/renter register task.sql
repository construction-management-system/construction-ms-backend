-- START of our project SEQUENCES
create sequence renter_sequence;

-- END of our project SEQUENCES....

CREATE OR REPLACE package renter_package As 

--  START of renter data checking function declaretion

FUNCTION renter_data_validation (
        FULL_NAME in RENTER.FULL_NAME%type
    ) return VALIDATION_PACKAGE.increamental_object;
    
    --  END of renter data checking function declaretion
    
    --  START of renter registration function declaretion
    FUNCTION renter_registration(
        R_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        R_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        R_CITY in ADDRESS.CITY%TYPE,
        R_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        R_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        R_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        R_PHONE in CONTACT.PHONE%TYPE, 
        R_EMAIL in CONTACT.EMAIL%TYPE,  
        R_FULL_NAME in RENTER.FULL_NAME%type
    ) return VALIDATION_PACKAGE.increamental_object;
    
    --  END of customer registration function declaretion
    
    --  START of update renter function declaretion
    FUNCTION update_renter(
        U_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        U_RENTER_ID IN RENTER.RENTER_ID%TYPE,
        U_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        U_CITY in ADDRESS.CITY%TYPE,
        U_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        U_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        U_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        U_PHONE in CONTACT.PHONE%TYPE, 
        U_EMAIL in CONTACT.EMAIL%TYPE,
        U_FULL_NAME in RENTER.FULL_NAME%type
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
--  END of update renter function declaretion

--  START of delete renter function declaretion
    FUNCTION delete_renter(
        D_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        D_RENTER_ID IN RENTER.RENTER_ID%TYPE
    ) RETURN  VALIDATION_PACKAGE.increamental_object;
--  END of delete renter function declaretion

END renter_package;
-- END OF PACKAGE SPECIFICATION ...
/

-- START of PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY renter_package AS 
--- START of renter data validation  function initialization
    FUNCTION renter_data_validation(
        FULL_NAME in RENTER.FULL_NAME%type
    ) RETURN  VALIDATION_PACKAGE.increamental_object IS 
    
    variable_array increamental_array;
    variable_name_array increamental_array;
    regex_array increamental_array;
    error_info_array increamental_array;
    
    success_object  VALIDATION_PACKAGE.increamental_object;
    error_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
        variable_array := increamental_array(FULL_NAME);
        variable_name_array := increamental_array('FULL_NAME');
        regex_array := increamental_array(
                                    '^[[:alpha:][:space:]]{1,256}$'
                                    );
        error_info_array := increamental_array(
                                    'FULL NAME ERROR'
                                    );
        error_object := VALIDATION_PACKAGE.DATA_VALIDATION(variable_array, variable_name_array, regex_array, error_info_array);
        
        IF error_object.COUNT > 0 THEN
            RAISE validation_error;
        ELSE
            success_object('renter') := 'succeed';
        END IF;
                RETURN success_object; 
        EXCEPTION 
        WHEN validation_error THEN
            RETURN error_object;
        END;
        --- END of renter data validating function initialization  
        
        --  START of renter registration function initialization
        FUNCTION renter_registration(
            R_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
            R_COUNTRY in ADDRESS.COUNTRY%TYPE, 
            R_CITY in ADDRESS.CITY%TYPE,
            R_DISTRICT in ADDRESS.DISTRICT%TYPE, 
            R_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
            R_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
            R_PHONE in CONTACT.PHONE%TYPE, 
            R_EMAIL in CONTACT.EMAIL%TYPE,  
            R_FULL_NAME in RENTER.FULL_NAME%type
        ) RETURN VALIDATION_PACKAGE.increamental_object IS
        
    contact_id NUMBER;
    address_id NUMBER;
    renter_id NUMBER;
    
    user_role_id NUMBER;
    user_role_name NVARCHAR2(256);

    success_object  VALIDATION_PACKAGE.increamental_object;
    response_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
    
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = R_USER_ID;
        Select ROLE.NAME into user_role_name from role where role.ROLE_ID = user_role_id;
        
    If user_role_name = 'admin' or user_role_name = 'secretary' THEN
    
        response_object := ADDRESS_PACKAGE.REGISTER_ADDRESS(renter_registration.R_COUNTRY, renter_registration.R_CITY, renter_registration.R_DISTRICT, renter_registration.R_HOME_NUMBER, renter_registration.R_MAP_LOCATION);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_address_id' THEN
            RAISE validation_error;
        ELSE
            renter_registration.address_id := response_object(response_object.FIRST);
        END IF;
        
        response_object := CONTACT_PACKAGE.REGISTER_CONTACT(renter_registration.R_PHONE, renter_registration.R_EMAIL, renter_registration.address_id);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'registered_contact_id' THEN
            RAISE validation_error;
        ELSE
            renter_registration.contact_id := response_object(response_object.FIRST);
        END IF;
        
    response_object := RENTER_PACKAGE.RENTER_DATA_VALIDATION(renter_registration.R_FULL_NAME);
    IF response_object.COUNT > 0 AND response_object(response_object.FIRST) != 'succeed' THEN
        RAISE validation_error;
    ELSE
        INSERT INTO RENTER
        VALUES (renter_sequence.NEXTVAL, renter_registration.R_FULL_NAME, renter_registration.CONTACT_ID)
        RETURNING RENTER.RENTER_ID INTO renter_registration.renter_id;
        success_object('register_renter_id') := renter_registration.renter_id;
        COMMIT;
        
    END IF;
    
    ELSE
        response_object('ERROR') := 'You do not have permission';
        RAISE validation_error;
    END IF;
        RETURN success_object; 
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            response_object('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN
            response_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN response_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN
            response_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN response_object;
    END;
--  END of renter data checking function initialization

-- START OF UPDATE RENTER FUNCTION INITIALIZATION -----
    FUNCTION update_renter (
        U_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        U_RENTER_ID IN RENTER.RENTER_ID%TYPE,
        U_COUNTRY in ADDRESS.COUNTRY%TYPE, 
        U_CITY in ADDRESS.CITY%TYPE,
        U_DISTRICT in ADDRESS.DISTRICT%TYPE, 
        U_HOME_NUMBER in ADDRESS.HOME_NUMBER%TYPE,
        U_MAP_LOCATION in ADDRESS.MAP_LOCATION%TYPE,
        U_PHONE in CONTACT.PHONE%TYPE, 
        U_EMAIL in CONTACT.EMAIL%TYPE,
        U_FULL_NAME in RENTER.FULL_NAME%type
    ) RETURN VALIDATION_PACKAGE.increamental_object IS
        
    contact_id NUMBER;
    renter_id NUMBER;
    address_id NUMBER;
    
    user_role_id NUMBER;
    user_role_name NVARCHAR2(256);

    success_object  VALIDATION_PACKAGE.increamental_object;
    response_object  VALIDATION_PACKAGE.increamental_object;
    
    validation_error EXCEPTION;
    BEGIN
    
        Select EMPLOYEE.ROLE_ID into user_role_id from employee where employee.employee_id = U_USER_ID;
        Select ROLE.NAME into user_role_name from role where role.ROLE_ID = user_role_id;
        
    If user_role_name = 'admin' or user_role_name = 'secretary' THEN
    
        select RENTER.CONTACT_ID into update_renter.contact_id from RENTER where RENTER.RENTER_ID = update_renter.U_RENTER_ID;            
        select CONTACT.ADDRESS_ID into update_renter.address_id from CONTACT where CONTACT.CONTACT_ID = update_renter.contact_id;
    
        response_object := ADDRESS_PACKAGE.UPDATE_ADDRESS(update_renter.address_id, update_renter.U_COUNTRY, update_renter.U_CITY, update_renter.U_DISTRICT, update_renter.U_HOME_NUMBER, update_renter.U_MAP_LOCATION);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'updated_address_id' THEN
            RAISE validation_error;
        END IF;
        
        response_object := CONTACT_PACKAGE.UPDATE_CONTACT(update_renter.contact_id, update_renter.U_PHONE, update_renter.U_EMAIL, update_renter.address_id);
        IF response_object.COUNT > 0 AND response_object.FIRST != 'updated_contact_id' THEN
            RAISE validation_error;
        END IF;
        
    response_object := RENTER_PACKAGE.RENTER_DATA_VALIDATION(update_renter.U_FULL_NAME);
    IF response_object.COUNT > 0 AND response_object(response_object.FIRST) != 'succeed' THEN
        RAISE validation_error;
    ELSE
        UPDATE RENTER SET 
            RENTER.FULL_NAME  = update_renter.U_FULL_NAME,
            RENTER.CONTACT_ID = update_renter.CONTACT_ID
        WHERE RENTER.RENTER_ID = update_renter.U_RENTER_ID;
        success_object('renter_updated') := 'succeed';
        COMMIT;
        
    END IF;
    
    ELSE
        response_object('ERROR') := 'You do not have permission';
        RAISE validation_error;
    END IF;
        RETURN success_object; 
    EXCEPTION 
        WHEN TOO_MANY_ROWS THEN
            ROLLBACK;
            response_object('ERROR') := 'Too many records returned';
            RETURN response_object;
        WHEN NO_DATA_FOUND THEN
            response_object('ERROR') := 'No record found';
            ROLLBACK;
            RETURN response_object;
        WHEN validation_error THEN
            ROLLBACK;
            RETURN response_object;
        WHEN OTHERS THEN
            response_object('ERROR') := 'Some thing is wrong';
            ROLLBACK;
            RETURN response_object;
END;

--  END of update renter function initialization 

--  START of delete renter function initialization 

FUNCTION delete_renter (
        D_USER_ID IN EMPLOYEE.EMPLOYEE_ID%TYPE,
        D_RENTER_ID IN RENTER.RENTER_ID%TYPE
    ) RETURN VALIDATION_PACKAGE.increamental_object IS
    
    contact_id NUMBER;
    
    user_role_id number;
    
    user_role_name nvarchar2(256);
    
    success_object VALIDATION_PACKAGE.increamental_object;
    response_object VALIDATION_PACKAGE.increamental_object;
    
validation_error EXCEPTION;
BEGIN
    SELECT EMPLOYEE.ROLE_ID INTO delete_renter.user_role_id FROM EMPLOYEE 
    WHERE EMPLOYEE.EMPLOYEE_ID = delete_renter.D_USER_ID;
    Select ROLE.NAME into user_role_name from role where role.ROLE_ID = user_role_id;
    
IF delete_renter.user_role_name = 'admin' THEN

    DELETE FROM RENTER 
    WHERE RENTER.RENTER_ID = delete_renter.D_RENTER_ID;
    
    success_object ('renter_deleted') := 'succeed';
    
    COMMIT;
        ELSE response_object ('ERROR') := 'You do not have permission';

    RAISE validation_error;

    END IF;

    RETURN success_object;

    EXCEPTION
        WHEN TOO_MANY_ROWS THEN ROLLBACK;

        response_object ('ERROR') := 'Too many records returned';

    RETURN response_object;

        WHEN NO_DATA_FOUND THEN response_object ('ERROR') := 'No record found';

        ROLLBACK;

    RETURN response_object;

        WHEN OTHERS THEN response_object ('ERROR') := 'Some thing is wrong';

        ROLLBACK;

    RETURN response_object;
    
END;  

--  END of delete renter function initialization    

END renter_package; 
-- END of PACKAGE BODY
--------------------- END of renter TABLE or PACKAGE--------------------------
    