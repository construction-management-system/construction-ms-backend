CREATE TABLE address(
  address_id number,
  country nvarchar2(256),
  city nvarchar2(256),
  district nvarchar2(256),
  home_number number default(0),
  map_location nvarchar2(512),
  CONSTRAINT pk_address_id PRIMARY KEY (address_id)
);

CREATE TABLE contact (
  contact_id number,
  phone nvarchar2(16) not null,
  email nvarchar2(256) unique,
  address_id number,
  CONSTRAINT pk_contact_id PRIMARY KEY (contact_id),
  CONSTRAINT fk_contact_address_id FOREIGN KEY (address_id) REFERENCES address(address_id) ON DELETE
  SET
    NULL
);

CREATE TABLE role (
  role_id number,
  name nvarchar2(256) not null unique,
  CONSTRAINT pk_role_id PRIMARY KEY (role_id)
);

CREATE TABLE shift (
  shift_id number,
  name nvarchar2(256) not null unique,
  CONSTRAINT pk_shift_id PRIMARY KEY (shift_id)
);

CREATE TABLE job (
  job_id number,
  name nvarchar2(256) not null unique,
  CONSTRAINT pk_job_id PRIMARY KEY (job_id)
);

CREATE TABLE state (
  state_id number,
  name nvarchar2(256) not null unique,
  CONSTRAINT pk_state_id PRIMARY KEY (state_id)
);

CREATE TABLE deadline (
  deadline_id number,
  start_date date default sysdate,
  due_date date,
  CONSTRAINT pk_deadline_id PRIMARY KEY (deadline_id)
);

CREATE TABLE project_type (
  project_type_id number,
  name nvarchar2(256) not null unique,
  CONSTRAINT pk_project_type_id PRIMARY KEY (project_type_id)
);



CREATE TABLE employee (
  employee_id number,
  first_name nvarchar2(256) not null,
  last_name nvarchar2(256) not null,
  ssn number unique not null,
  photo nvarchar2(1024),
  hire_date date default sysdate,
  contact_id number,
  role_id number not null,
  shift_id number not null,
  job_id number not null,
  salary number default(0),
  has_password number(1,0) default(0), 
  contract_file nvarchar2(1024) not null,
  cv_link nvarchar2(1024),
  CONSTRAINT pk_employee_id PRIMARY KEY (employee_id),
  constraint fk_employee_contact_id foreign key (contact_id) references contact(contact_id) ON DELETE
  SET
    NULL,
    constraint fk_employee_role_id foreign key (role_id) references role(role_id),
    constraint fk_employee_shift_id foreign key (shift_id) references shift(shift_id),
    constraint fk_employee_job_id foreign key (job_id) references job(job_id)
);

CREATE TABLE auth (
  auth_id number,
  password varchar2(1024) not null,
  employee_id number unique not null,
  two_factor number(1, 0) default(0),
  createdAt date default sysdate,
  updatedAt date default sysdate,
  CONSTRAINT pk_auth_id PRIMARY KEY (auth_id),
  CONSTRAINT fk_employee_auth_id FOREIGN KEY (employee_id) references employee(employee_id) on delete cascade
);

CREATE TABLE customer (
  customer_id number,
  first_name nvarchar2(256) not null,
  last_name nvarchar2(256) not null,
  ssn number not null,
  contact_id number,
  photo nvarchar2(1024),
  registration_date date default sysdate,
  CONSTRAINT pk_customer_id PRIMARY KEY (customer_id),
  constraint fk_customer_contact_id foreign key (contact_id) references contact(contact_id) ON DELETE
  SET
    NULL
);

CREATE TABLE renter (
  renter_id number,
  full_name nvarchar2(512) not null,
  contact_id number,
  CONSTRAINT pk_renter_id PRIMARY KEY (renter_id),
  constraint fk_renter_contact_id foreign key (contact_id) references contact(contact_id) ON DELETE
  SET
    NULL
);

CREATE TABLE project (
  project_id number,
  name nvarchar2(256) not null,
  manager_id number not null,
  supervisor_id number,
  deadline_id number not null,
  state_id number not null,
  project_type_id number not null,
  progress number default(0),
  address_id number not null,
  cost number not null,
  contract_file nvarchar2(1024) not null,
  CONSTRAINT pk_project_id PRIMARY KEY (project_id),
  constraint fk_project_manager_id foreign key (manager_id) references employee(employee_id),
  constraint fk_project_supervisor_id foreign key (supervisor_id) references employee(employee_id) ON DELETE
  SET
    NULL,
    constraint fk_project_deadline_id foreign key (deadline_id) references deadline(deadline_id),
    constraint fk_project_state_id foreign key (state_id) references state(state_id),
    constraint fk_project_project_type_id foreign key (project_type_id) references project_type(project_type_id),
    CONSTRAINT fk_project_address_id FOREIGN KEY (address_id) REFERENCES address(address_id)
);

CREATE TABLE payment(
  payment_id number,
  paid number default(0),
  payment_date date default sysdate,
  project_id number not null,
  customer_id number not null,
  CONSTRAINT pk_payment_id PRIMARY KEY (payment_id),
  CONSTRAINT fk_payment_project_id FOREIGN KEY (project_id) REFERENCES project (project_id),
  CONSTRAINT fk_payment_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

CREATE TABLE project_customers (
  project_customers_id number,
  customer_id number not null,
  project_id number not null,
  registration_date date default sysdate,
  CONSTRAINT pk_project_customers_id PRIMARY KEY (project_customers_id),
  CONSTRAINT fk_pro_customers_customer_id FOREIGN KEY (customer_id) REFERENCES customer (customer_id) ON DELETE CASCADE,
  CONSTRAINT fk_pro_customers_project_id FOREIGN KEY (project_id) REFERENCES project (project_id) ON DELETE CASCADE
);

CREATE TABLE team (
  team_id number,
  name nvarchar2(256) not null,
  team_leader_id number not null,
  deadline_id number not null,
  CONSTRAINT pk_team_id PRIMARY KEY (team_id),
  CONSTRAINT fk_team_team_leader_id FOREIGN KEY (team_leader_id) REFERENCES employee (employee_id),
  CONSTRAINT fk_team_deadline_id FOREIGN KEY (deadline_id) REFERENCES deadline(deadline_id)
);

CREATE TABLE team_member (
  team_member_id number,
  employee_id number not null,
  team_id number not null,
  deadline_id number not null,
  CONSTRAINT pk_team_member_id PRIMARY KEY (team_member_id),
  constraint fk_team_member_employee_id foreign key (employee_id) references employee(employee_id),
  constraint fk_team_member_team_id foreign key (team_id) references team(team_id),
  CONSTRAINT fk_team_member_deadline_id FOREIGN KEY (deadline_id) REFERENCES deadline(deadline_id)
);

CREATE TABLE task(
  task_id number,
  title nvarchar2(256) not null,
  description nvarchar2(2000),
  team_id number,
  deadline_id number not null,
  progress number default(0),
  state_id number not null,
  project_id number not null,
  CONSTRAINT pk_task_id PRIMARY KEY (task_id),
  constraint fk_task_team_id foreign key (team_id) references team(team_id) ON DELETE
  SET
    NULL,
    constraint fk_task_deadline_id foreign key (deadline_id) references deadline(deadline_id),
    constraint fk_task_state_id foreign key (state_id) references state(state_id),
    constraint fk_task_project_id foreign key (project_id) references project(project_id)
);

CREATE TABLE sub_task (
  sub_task_id number,
  title nvarchar2(256) not null,
  description nvarchar2(2000),
  deadline_id number not null,
  task_id number not null,
  state_id number not null,
  CONSTRAINT pk_sub_task_id PRIMARY KEY (sub_task_id),
  constraint fk_sub_task_deadline_id foreign key (deadline_id) references deadline(deadline_id),
  constraint fk_sub_task_task_id foreign key (task_id) references task(task_id) on delete cascade,
  constraint fk_sub_task_state_id foreign key (state_id) references state(state_id)
);

CREATE TABLE equipment (
  equipment_id number,
  name nvarchar2(256) not null,
  description nvarchar2(2000),
  registeration_date date default sysdate,
  price number not null,
  count_number number not null,
  CONSTRAINT pk_equipment_id PRIMARY KEY (equipment_id)
);

CREATE TABLE machinary (
  machinary_id number,
  name nvarchar2(256) not null,
  model nvarchar2(256),
  price number not null,
  registeration_date date default sysdate,
  documents nvarchar2(1024),
  CONSTRAINT pk_matchinary_id PRIMARY KEY (machinary_id)
);

CREATE TABLE task_requirement(
  task_requirement_id number,
  task_id number not null,
  equipment_id number,
  machinary_id number,
  count number not null,
  loss number default(0),
  returned number(1, 0) default(0),
  CONSTRAINT pk_task_requirement_id PRIMARY KEY(task_requirement_id),
  constraint fk_task_req_task_id foreign key (task_id) references task(task_id),
  constraint fk_task_req_matchinary_id foreign key (machinary_id) references machinary(machinary_id),
  constraint fk_task_req_equipment_id foreign key (equipment_id) references equipment(equipment_id)
);

CREATE TABLE rented(
  rented_id NUMBER,
  renter_id NUMBER not null,
  task_id number not null,
  name nvarchar2(256) not null,
  cost number not null,
  count number default(1),
  deadline_id number not null,
  CONSTRAINT pk_rented_id PRIMARY KEY(rented_id),
  constraint fk_rented_renter_id foreign key (renter_id) references renter(renter_id),
  constraint fk_rented_deadline_id foreign key (deadline_id) references deadline(deadline_id),
  constraint fk_rented_task_id foreign key (task_id) references task(task_id)
);

CREATE TABLE expense_type(
  expense_type_id number,
  name nvarchar2(50),
  CONSTRAINT pk_expense_type_id PRIMARY KEY (expense_type_id)
);

CREATE TABLE expense (
  expense_id number,
  name nvarchar2(256) not null,
  description nvarchar2(2000) not null,
  expense_type_id number not null,
  expense_date date default sysdate,
  amount number default(0),
  bill_info nvarchar2(1024),
  task_id number,
  CONSTRAINT pk_expense_id PRIMARY KEY (expense_id),
  CONSTRAINT fk_expense_type_id FOREIGN KEY (expense_type_id) references expense_type(expense_type_id),
  constraint fk_expense_task_id foreign key (task_id) references task(task_id) ON DELETE
  SET
    NULL
);

CREATE TABLE chat (
  chat_id number,
  sender_id number,
  reciever_id number,
  join_date date not null,
  CONSTRAINT pk_chat_id PRIMARY KEY (chat_id),
  constraint fk_chat_sender_id foreign key (sender_id) references employee(employee_id) ON DELETE
  SET
    NULL,
    constraint fk_chat_reciever_id foreign key (reciever_id) references employee(employee_id) ON DELETE
  SET
    NULL
);

CREATE TABLE message (
  message_id number,
  is_from_sender number(1, 0) default(0),
  chat_id number not null,
  date_time date not null,
  content nvarchar2(2000) not null,
  CONSTRAINT pk_message_id PRIMARY KEY (message_id),
  constraint fk_message_chat_id foreign key (chat_id) references chat(chat_id)
);

CREATE TABLE notification(
  notification_id number,
  receiver_id number not null,
  sender_id number not null,
  recieved_date date not null,
  subject nvarchar2(256) not null,
  description nvarchar2(2000) not null,
  CONSTRAINT pk_notification_id PRIMARY KEY (notification_id),
  constraint fk_notification_receiver_id foreign key (receiver_id) references employee(employee_id),
  constraint fk_notification_sender_id foreign key (sender_id) references employee(employee_id)
);

/*
 CREATE TABLE logger(
 logger_id number,
 employee_id number,
 action nvarchar2(2048),
 log_date date default sysdate,
 CONSTRAINT pk_logger_id PRIMARY KEY (logger_id)
 );
 */